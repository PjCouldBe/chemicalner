package ru.itis.pipelines.main;

import ru.itis.general.Token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Dima on 24.08.2016.
 */
public class CharacterTokenizer {
    private static final String SYMBOL_TOKENS =
            "[~•!@#$%^&*-=_+ˉ\\(\\)\\[\\]\\{\\};':\",./<>×><≤≥↑↓←→•′°~≈?Δ÷≠|‘’“”§£€\\012345678]";
    private static final String SPACES = "\\s+";

    public Collection<String> tokenize(String text) {
        final Pattern symbolPattern = Pattern.compile(SYMBOL_TOKENS, Pattern.MULTILINE);
        List<String> resultTokens = new ArrayList<>(text.length() / 7);
        List<String> simpleTokens = Arrays.asList( text.split(SPACES) );

        for (String simpleToken : simpleTokens) {
            Matcher symbolMatcher = symbolPattern.matcher(simpleToken);
            int lastStart = 0;

            while (symbolMatcher.find()) {
                if (symbolMatcher.start() > 0 && symbolMatcher.start() != lastStart) {
                    resultTokens.add( simpleToken.substring(lastStart, symbolMatcher.start()) );
                }
                resultTokens.add(symbolMatcher.group());
                lastStart = symbolMatcher.end();
            }

            if (lastStart < simpleToken.length()) {
                resultTokens.add( simpleToken.substring(lastStart) );
            }
        }

        return resultTokens;
    }
}
