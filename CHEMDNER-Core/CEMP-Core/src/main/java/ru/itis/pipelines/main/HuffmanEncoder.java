package ru.itis.pipelines.main;

import gnu.trove.TDoubleObjectHashMap;
import gnu.trove.TObjectLongHashMap;

import java.util.*;

/**
 * Created by Dima on 25.08.2016.
 */
public class HuffmanEncoder<T> {
    private BitSet encodings = new BitSet();
    private TObjectLongHashMap<T> objectToEncodePositions = new TObjectLongHashMap<>();

    public HuffmanEncoder(Map<T, Integer> distributionMap) {
        //inverse and sort inputs to get frequencies to objects matching
        TreeMap<Integer, T> freqs2Objs = new TreeMap<>(Integer::compare);
        distributionMap.entrySet()
                .parallelStream()
                .forEach(e -> freqs2Objs.put(new Integer(e.getValue()), e.getKey()));

        //make a aorted array of frequency to object pairs and only frequencies sequence to manage with it
        int len = freqs2Objs.size();
        AbstractMap.SimpleEntry<Integer, T>[] freqs2ObjsArray = new AbstractMap.SimpleEntry[len];
        int[] onlyFreqs = new int[len];
        for (int i = 0; i < len; i++) {
            freqs2ObjsArray[i] = (AbstractMap.SimpleEntry<Integer, T>)freqs2Objs.pollFirstEntry();
            onlyFreqs[i] = freqs2ObjsArray[i].getKey();
        }

        //compute sequence of intermediate sums
        BitSet wasUsed = new BitSet(len);
        int[] sumSequence = new int[2 * len - 2];
        int srcI, targetI;  //left and right augend respectively
        for (int i = 0; i < len - 2; i++) {  //(n - 1) - 1 because 2 numbers taken
            if (onlyFreqs[i] <= onlyFreqs[i+1]) {
                if (onlyFreqs[i+1] <= onlyFreqs[i+2]) {
                    srcI = i;
                    targetI = i + 1;
                } else {
                    srcI = i;
                    targetI = i + 2;
                }
            } else {
                if (onlyFreqs[i] <= onlyFreqs[i+2]) {
                    srcI = i;
                    targetI = i + 1;
                } else {
                    srcI = i + 1;
                    targetI = i + 2;
                }
            }

            onlyFreqs[targetI] += onlyFreqs[srcI];
            sumSequence[2*i] = srcI;  sumSequence[2*i + 1] = targetI;
            wasUsed.set(srcI);
        }
    }
}
