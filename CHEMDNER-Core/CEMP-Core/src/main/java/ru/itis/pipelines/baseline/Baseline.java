package ru.itis.pipelines.baseline;

import ru.itis.CEMPBaseModel;
import ru.itis.config.MLPipeline;
import ru.itis.config.NERPipeline;
import ru.itis.data.Input;
import ru.itis.evaluation.BioCreativeScriptEvaluation;
import ru.itis.general.BaselineNLPPipeline;
import ru.itis.input.ChemicalInput;
import ru.itis.input.ChemicalNormalizedInput;
import ru.itis.learning.SupervisedLearningMethod;
import ru.itis.mark.CEMPAnnotation;
import ru.itis.tools.FileAnnotationCorpusUtils;
import ru.itis.tools.FileTextCorpusUtils;
import ru.itis.tools.RawToGroupedAnnotationsConverter;

import java.util.List;

/**
 * Created by Dima on 13.07.2016.
 */
public class Baseline {
    public static MLPipeline create() {
        Input<ChemicalNormalizedInput> inputs = FileTextCorpusUtils.getCEMPTrainingSet();
        List<CEMPAnnotation[]> trainingAnnotations =
                RawToGroupedAnnotationsConverter.convert(
                    inputs.builder().getInputCollection(ChemicalInput.class),
                    FileAnnotationCorpusUtils.getTrainingAnnotations_CEMP()
                );

        return NERPipeline.create(CEMPAnnotation.class)
                .configureInputs(inputs)
                .confgureLearningMethod(trainingAnnotations, new SupervisedLearningMethod<>())
                .configureLearningAlgorithm( new CEMPBaseModel() )
                .configureNLPPipeline( new BaselineNLPPipeline("CEMPDictionary.txt") )
                .configureEvaluation( new BioCreativeScriptEvaluation() )
                .endConfiguration();
    }
}
