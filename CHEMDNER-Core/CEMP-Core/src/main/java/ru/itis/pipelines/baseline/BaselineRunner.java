package ru.itis.pipelines.baseline;

import ru.itis.config.MLPipeline;
import ru.itis.data.Input;
import ru.itis.data.InputToOutputRule;
import ru.itis.data.NormalizeInputRule;
import ru.itis.evaluate.Evaluation;
import ru.itis.evaluation.ChemicalAnnotationToEvalPathConverter;
import ru.itis.input.ChemicalInput;
import ru.itis.input.ChemicalNormalizedInput;
import ru.itis.mark.CEMPAnnotation;
import ru.itis.model.TrainedModel;
import ru.itis.output.ChemicalAnnotatedOutput;
import ru.itis.tools.FileTextCorpusUtils;
import ru.itis.utils.ResourceUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dima on 13.07.2016.
 */
public class BaselineRunner {
    private void run() {
        MLPipeline pipeline = Baseline.create();
        TrainedModel<ChemicalNormalizedInput, CEMPAnnotation, String> model = pipeline.run();

        //evaluating by model
        Input<ChemicalNormalizedInput> debugSet = FileTextCorpusUtils.getDevelopmentSet("debugCEMP");
        List<String> debugAnnotations = new ArrayList<>();
        debugAnnotations.add(ResourceUtils.getResourceAsFile(
                "annotationsets/development/debugCEMPAnnotationsEval.tsv").getPath());
        Evaluation evaluation = model.evaluate(
                debugSet,
                debugAnnotations, //FileAnnotationCorpusUtils.getDevelomentAnnotationSet_CEMP("debugCEMPAnnotations.tsv"),
                new ChemicalAnnotationToEvalPathConverter<CEMPAnnotation>()::convert);
        evaluation.printAllCharacteristics();


        //annotating by model
        List<CEMPAnnotation> annotations = model.apply(debugSet);
        String outputFileName = "CEMP-Core/src/main/resources/output/CEMPoutputAnnotations.tsv";
        try (PrintWriter pw = new PrintWriter(
                                  new BufferedWriter(
                                      new FileWriter( outputFileName )))) {
            for (CEMPAnnotation ann : annotations) {
                pw.println( ann.toString() );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        //applying model for predictions
        Input<ChemicalNormalizedInput> testSet = FileTextCorpusUtils.getTestSet("textSet");
        List<ChemicalInput> inputs = new ArrayList<>(testSet.builder().getInputCollection(ChemicalInput.class));
        NormalizeInputRule<ChemicalInput, ChemicalNormalizedInput>[] rules =
                new NormalizeInputRule[] {ChemicalInput.NORMALIZE_BY_TITLE_RULE, ChemicalInput.NORMALIZE_BY_ABSTRACT_RULE};
        InputToOutputRule<ChemicalInput, CEMPAnnotation, ChemicalAnnotatedOutput> ioRule =
                (InputToOutputRule<ChemicalInput, CEMPAnnotation, ChemicalAnnotatedOutput>)ChemicalAnnotatedOutput.TO_ANNOTATED_RULE;

        List<ChemicalAnnotatedOutput> annotatedOutputs = model.use(inputs, rules, ioRule);
        outputFileName = "CEMP-Core/src/main/resources/output/CEMP_AnnotatedOutputs.txt";
        try (PrintWriter pw = new PrintWriter(
                                new BufferedWriter(
                                    new FileWriter(outputFileName)))) {
            for (ChemicalAnnotatedOutput aa : annotatedOutputs) {
                pw.println( aa.toString() );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new BaselineRunner().run();
    }
}
