package ru.itis.pipelines.main;

/**
 * Created by Dima on 25.08.2016.
 */
public class Inverter {
    public String invert(String str) {
        return new StringBuilder(str).reverse().toString();
    }
}
