package ru.itis.pipelines.main;

import javax.annotation.Nonnull;
import static java.lang.Math.min;

/**
 * Created by Dima on 26.08.2016.
 */
public class HuffmanDSU {
    private int[] sets;
    private int[] weights;
    private String[] encodes;

    public HuffmanDSU(@Nonnull int[] weights) {
        final int len = weights.length;
        this.weights = weights;
        this.sets = new int[len];
        this.encodes = new String[len];

        for (int i = 0; i < weights.length; i++) {
            sets[i] = i;  //makeSet(i)
            encodes[i] = "".intern();
        }
    }

    public void build() {

    }

    public int find(int i) {
        return sets[i];
    }

    private long findMinimums(int start) {  //union 2 int in one long
        int[] inds = {start, -1, -1};
        for (int j = 0; j < 3; j++) {
            while (find(inds[j]) != inds[j]) {
                inds[j] = find(inds[j]);
            }
            if (j != 2) {
                inds[j+1] = inds[j] + 1;
            }
        }

        int i1 = inds[0], i2 = inds[1], i3 = inds[2];
        if (i1 < i2 && i1 < i3) {
            return ((long)i1 << 32) | min(i2, i3);
        }
        /*if (inds[0] == inds[1] || inds[0] == inds[2]) {
            return -1; //TODO: пролетаем
        } else if (inds[1] == inds[2]) {
            return ((long)inds[1] << 32) | inds[2];
        } else {
            return ;
        }*/

        return -1;
    }

    private void union(int left, int right) {
        sets[left] = right;
        weights[right] += weights[left];
        encodes[left] += "0";
        encodes[right] += "1";
    }
}
