package ru.itis;

import ru.itis.data.CEMPOutputBatch;
import ru.itis.data.ChemicalOutputBatch;
import ru.itis.data.OutputBatch;
import ru.itis.general.ChemicalModelAlgorithm;
import ru.itis.general.Token;
import ru.itis.input.ChemicalNormalizedInput;
import ru.itis.mark.CEMPAnnotation;
import ru.itis.mark.ChemicalAnnotationType;
import ru.itis.preprocess.FeatureSet;

import java.util.List;

/**
 * Created by Filippov on 14.07.2016.
 */
public class CEMPBaseModel extends ChemicalModelAlgorithm<CEMPAnnotation> {
    @Override
    protected <C extends ChemicalAnnotationType> C produceAnnotationType(ChemicalNormalizedInput input, Token t) {
        CEMPAnnotation.CEMType type = t.getText().matches("[a-zA-z]")
                                            ? CEMPAnnotation.CEMType.TRIVIAL
                                            : CEMPAnnotation.CEMType.SYSTEMATIC;
        return (C)type;
    }

    @Override
    protected <T extends OutputBatch<CEMPAnnotation>> T createOutputBatch(
            String[] patentIDs, String[] titleTexts, String[] abstractTexts,
            List<ChemicalOutputBatch.ChemicalBatchNonTextData[]> batchData)
    {
        return (T)new CEMPOutputBatch(patentIDs, titleTexts, abstractTexts, batchData);
    }
}
