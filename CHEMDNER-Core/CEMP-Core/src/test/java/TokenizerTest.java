import junit.framework.Assert;
import org.junit.Test;
import ru.itis.pipelines.main.CharacterTokenizer;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Dima on 24.08.2016.
 */
public class TokenizerTest {
    @Test
    public void testTokenization() {
        final String text = "branched chain C1-C10 -alkyl group";
        Collection<String> expected = Arrays.asList("branched", "chain",
                "C", "1", "-", "C", "1", "0", "-", "alkyl", "group");
        Collection<String> actual = new CharacterTokenizer().tokenize(text);

        Assert.assertEquals(expected, actual);
    }
}
