import org.junit.Test;
import ru.itis.pipelines.main.Huffman;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dima on 26.08.2016.
 */
public class HuffmanTest {
    @Test
    public void testHuffmanSimply() {
        Huffman<String> huffman = new Huffman<>();

        Map<String, Integer> map = new HashMap<>();
        map.put("cluster1", 10);
        map.put("cluster2", 5);
        map.put("cluster3", 15);
        map.put("cluster4", 8);
        map.put("cluster5", 7);
        map.put("cluster6", 26);
        map.put("cluster7", 2);
        map.put("cluster8", 7);
        map.put("cluster9", 4);
        map.put("cluster10", 10);
        huffman.buildTree(map);

        List<String> inputs = Arrays.asList("cluster9", "cluster1", "cluster2", "cluster6", "cluster10", "cluster5");
        System.out.println( huffman.encodeMessage(inputs) );
    }
}
