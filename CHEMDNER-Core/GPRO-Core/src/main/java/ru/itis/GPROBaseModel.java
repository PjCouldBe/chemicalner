package ru.itis;

import ru.itis.data.ChemicalOutputBatch;
import ru.itis.data.GPROOutputBatch;
import ru.itis.data.OutputBatch;
import ru.itis.general.ChemicalModelAlgorithm;
import ru.itis.general.Token;
import ru.itis.input.ChemicalNormalizedInput;
import ru.itis.mark.ChemicalAnnotationType;
import ru.itis.mark.GPROAnnotation;
import ru.itis.mark.GPROType_C1;
import ru.itis.mark.GPROType_C2;
import ru.itis.mark.enums.C1Type;
import ru.itis.mark.enums.C2Type;

import java.util.List;

/**
 * Created by Filippov on 14.07.2016.
 */
public class GPROBaseModel extends ChemicalModelAlgorithm<GPROAnnotation> {
    @Override
    protected <C extends ChemicalAnnotationType> C produceAnnotationType(ChemicalNormalizedInput input, Token t) {
        String text = t.getText();
        return (text.length() > 10
                    ? (C)new GPROType_C1(C1Type.FULL_NAME)
                    : (text.length() < 6 || text.matches("\\w*\\d+\\w*")
                            ? (C)new GPROType_C1(C1Type.ABBREVIATION)
                            : (C)new GPROType_C2(C2Type.NO_CLASS)));
    }

    @Override
    protected <T extends OutputBatch<GPROAnnotation>> T createOutputBatch(
            String[] patentIDs, String[] titleTexts, String[] abstractTexts,
            List<ChemicalOutputBatch.ChemicalBatchNonTextData[]> batchData)
    {
        return (T)new GPROOutputBatch(patentIDs, titleTexts, abstractTexts, batchData);
    }
}
