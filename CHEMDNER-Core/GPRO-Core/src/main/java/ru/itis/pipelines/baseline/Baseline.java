package ru.itis.pipelines.baseline;

import ru.itis.GPROBaseModel;
import ru.itis.config.MLPipeline;
import ru.itis.config.NERPipeline;
import ru.itis.data.Input;
import ru.itis.evaluation.BioCreativeScriptEvaluation;
import ru.itis.general.BaselineNLPPipeline;
import ru.itis.input.ChemicalInput;
import ru.itis.input.ChemicalNormalizedInput;
import ru.itis.learning.SupervisedLearningMethod;
import ru.itis.mark.GPROAnnotation;
import ru.itis.tools.FileAnnotationCorpusUtils;
import ru.itis.tools.FileTextCorpusUtils;
import ru.itis.tools.RawToGroupedAnnotationsConverter;

import java.util.List;

/**
 * Created by Dima on 13.07.2016.
 */
public class Baseline {
    public static MLPipeline create() {
        Input<ChemicalNormalizedInput> inputs = FileTextCorpusUtils.getGPROTrainingSet();
        List<GPROAnnotation[]> trainingAnnotations =
                RawToGroupedAnnotationsConverter.convert(
                    inputs.builder().getInputCollection(ChemicalInput.class),
                    FileAnnotationCorpusUtils.getTrainingAnnotations_GPRO()
                );
        
        return NERPipeline.create(GPROAnnotation.class)
                .configureInputs(inputs)
                .confgureLearningMethod(trainingAnnotations, new SupervisedLearningMethod<>())
                .configureLearningAlgorithm( new GPROBaseModel() )
                .configureNLPPipeline( new BaselineNLPPipeline("GPRODictionary.txt") )
                .configureEvaluation( new BioCreativeScriptEvaluation() )
                .endConfiguration();
    }
}
