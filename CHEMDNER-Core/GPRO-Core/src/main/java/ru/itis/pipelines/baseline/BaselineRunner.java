package ru.itis.pipelines.baseline;

import ru.itis.config.MLPipeline;
import ru.itis.data.Input;
import ru.itis.data.InputToOutputRule;
import ru.itis.data.NormalizeInputRule;
import ru.itis.evaluate.Evaluation;
import ru.itis.evaluation.ChemicalAnnotationToEvalPathConverter;
import ru.itis.input.ChemicalInput;
import ru.itis.input.ChemicalNormalizedInput;
import ru.itis.mark.GPROAnnotation;
import ru.itis.model.TrainedModel;
import ru.itis.output.ChemicalAnnotatedOutput;
import ru.itis.tools.FileTextCorpusUtils;
import ru.itis.utils.ResourceUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dima on 13.07.2016.
 */
public class BaselineRunner {
    private void run() {
        MLPipeline pipeline = Baseline.create();
        TrainedModel<ChemicalNormalizedInput, GPROAnnotation, String> model = pipeline.run();

        //evaluating by model
        Input<ChemicalNormalizedInput> debugSet = FileTextCorpusUtils.getDevelopmentSet("debugGPRO");
        List<String> debugAnnotations = new ArrayList<>();
        debugAnnotations.add(ResourceUtils.getResourceAsFile(
                "annotationsets/development/debugGPROAnnotationsEval.tsv").getPath());
        Evaluation evaluation = model.evaluate(
                debugSet,
                debugAnnotations, //FileAnnotationCorpusUtils.getDevelomentAnnotationSet_GPRO("debugGPROAnnotations.tsv"),
                new ChemicalAnnotationToEvalPathConverter<GPROAnnotation>()::convert);
        evaluation.printAllCharacteristics();


        //annotating by model
        List<GPROAnnotation> annotations = model.apply(debugSet);
        String outputFileName = "GPRO-Core/src/main/resources/output/GPROoutputAnnotations.tsv";
        //File f = new File(outputFileName);
        try (PrintWriter pw = new PrintWriter(
                new BufferedWriter(
                        new FileWriter( outputFileName )))) {
            for (GPROAnnotation ann : annotations) {
                pw.println( ann.toString() );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        //applying model for predictions
        Input<ChemicalNormalizedInput> testSet = FileTextCorpusUtils.getTestSet("textSet");
        List<ChemicalInput> inputs = new ArrayList<>(testSet.builder().getInputCollection(ChemicalInput.class));
        NormalizeInputRule<ChemicalInput, ChemicalNormalizedInput>[] rules =
                new NormalizeInputRule[] {ChemicalInput.NORMALIZE_BY_TITLE_RULE, ChemicalInput.NORMALIZE_BY_ABSTRACT_RULE};
        InputToOutputRule<ChemicalInput, GPROAnnotation, ChemicalAnnotatedOutput> ioRule =
                (InputToOutputRule<ChemicalInput, GPROAnnotation, ChemicalAnnotatedOutput>)ChemicalAnnotatedOutput.TO_ANNOTATED_RULE;

        List<ChemicalAnnotatedOutput> annotatedOutputs = model.use(inputs, rules, ioRule);
        outputFileName = "GPRO-Core/src/main/resources/output/GPRO_AnnotatedOutputs.txt";
        try (PrintWriter pw = new PrintWriter(
                                new BufferedWriter(
                                    new FileWriter(outputFileName)))) {
            for (ChemicalAnnotatedOutput aa : annotatedOutputs) {
                pw.println( aa.toString() );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        BaselineRunner baseline = new BaselineRunner();
        baseline.run();
    }
}
