package ru.itis.tools;

import ru.itis.data.Input;
import ru.itis.input.ChemicalNormalizedInput;
import ru.itis.input.InputProducer;
import ru.itis.utils.ResourceUtils;

import java.io.File;

/**
 * Created by Dima on 13.07.2016.
 */
public class FileTextCorpusUtils {
    public static final String CEMP_TRAINING_SET_PATH = "textsets/training/trainingCEMP.txt";
    public static final String GPRO_TRAINING_SET_PATH = "textsets/training/trainingGPRO.txt";

    public static Input<ChemicalNormalizedInput> getCEMPTrainingSet() {
        return getInputStoreFromFileResource(CEMP_TRAINING_SET_PATH);
    }

    public static Input<ChemicalNormalizedInput> getGPROTrainingSet() {
        return getInputStoreFromFileResource(GPRO_TRAINING_SET_PATH);
    }

    public static Input<ChemicalNormalizedInput> getAnotherTrainingSet(String fileName) {
        return getInputStoreFromFileResource("textsets/training/" + fileName + ".txt");
    }

    public static Input<ChemicalNormalizedInput> getDevelopmentSet(String fileName) {
        return getInputStoreFromFileResource("textsets/development/" + fileName + ".txt");
    }

    public static Input<ChemicalNormalizedInput> getTestSet(String fileName) {
        return getInputStoreFromFileResource("textsets/test/" + fileName + ".txt");
    }

    public static Input<ChemicalNormalizedInput> getExternalTextSet(String absoluteFilePath) {
        return InputProducer.produce( new File(absoluteFilePath) );
    }



    private static Input<ChemicalNormalizedInput> getInputStoreFromFileResource(String path) {
        return InputProducer.produce( ResourceUtils.getResourceAsFile(path) );
    }
}
