package ru.itis.tools;

import ru.itis.input.ChemicalInput;
import ru.itis.input.TextType;
import ru.itis.mark.CEMPAnnotation;
import ru.itis.utils.ResourceUtils;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Dima on 16.07.2016.
 */
public class TsvToPubTatorConverter {
    /**
     * Returns the path to output converted file.
     * Uses convert and saves the result to file system to main/resources folder
     *
     * @param srcTexts - input .tsv or .txt file with texts to be converted
     * @param srcAnnotations - input .tsv or .txt file with annotations to be converted with srcTexts
     * @return converted and saved file absolute path
     * @throws IllegalArgumentException - if argument file is not in .tsv or .txt format
     * @throws NullPointerException - if argument is Null
     */
    public String convertAndSave(@Nonnull File srcTexts, @Nonnull File srcAnnotations) {
        String resultPath = srcTexts.getPath()
                                    .replaceAll("\\.tsv$", ".PubTator")
                                    .replaceAll("\\.txt$", ".PubTator");

        try (FileWriter pw = new FileWriter(resultPath)) {
            pw.write( convert(srcTexts, srcAnnotations) );
            return resultPath;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Converts file in tab separated format to PubTator format.
     * Returns the converted result in String representation.
     *
     * @param srcTexts - input .tsv or .txt file with texts to be converted
     * @param srcAnnotations - input .tsv or .txt file with annotations to be converted with srcTexts
     * @return converted result in String representation
     * @throws IllegalArgumentException - if argument file is not in .tsv or .txt format
     * @throws NullPointerException - if argument is Null
     */
    public String convert(@Nonnull File srcTexts, @Nonnull File srcAnnotations) {
        String pathToTexts = srcTexts.getPath();
        String pathToAnns = srcAnnotations.getPath();
        if (pathToTexts.endsWith(".tsv") == false && pathToTexts.endsWith(".txt") == false
                || pathToAnns.endsWith(".tsv") == false && pathToAnns.endsWith(".txt") == false) {
            throw new IllegalArgumentException("File must be of .tsv or .txt format to handle it correctly");
        }

        Collection<ChemicalInput> inputTexts = FileTextCorpusUtils.getExternalTextSet(pathToTexts)
                .builder().getInputCollection(ChemicalInput.class);

        List<CEMPAnnotation> lstAnns = FileAnnotationCorpusUtils.getExternalAnnotationSet_CEMP(pathToAnns);
        Comparator<CEMPAnnotation> comp = Comparator.comparing(CEMPAnnotation::getTextType)
                .thenComparingInt(CEMPAnnotation::getStart).thenComparingInt(CEMPAnnotation::getEnd);
        Map<String, List<CEMPAnnotation>> annsMap = lstAnns.stream()
                .collect(Collectors.groupingBy(ann -> ann.getPatentId()));

        StringBuilder sb = new StringBuilder();
        return inputTexts.stream()
                    .filter(input -> annsMap.get(input.getPatentId()) != null)
                    .map(input -> {
                        String patId = input.getPatentId();
                        int titleLen = input.getTitleText().length();

                        List<CEMPAnnotation> valueList = annsMap.get(patId);
                        Collections.sort(valueList, comp);

                        return patId + "|t|" + input.getTitleText() + "\n"
                                + patId + "|a|" + input.getAbstractText() + "\n" +
                                valueList.stream().map(
                                        value -> patId + "\t" +
                                                (value.getTextType() == TextType.A
                                                        ? (value.getStart() + titleLen + 1) + "\t" + (value.getEnd() + titleLen + 1)
                                                        : value.getStart() + "\t" + value.getEnd()) +
                                                "\t" +
                                                value.getOriginalText() + "\t" +
                                                value.getType()
                                ).collect(Collectors.joining("\n"));
                    }).collect(Collectors.joining("\n\n"));


       /* String[] lastReadLine = new String[1];

        try (BufferedReader brTexts = new BufferedReader(
                new FileReader(srcTexts) ))
        {
            try (BufferedReader brAnns = new BufferedReader(
                    new FileReader(srcAnnotations) )) {
                String result = brTexts.lines().map(textLine -> {
                    String[] inputData = textLine.split("\\t");
                    if (inputData.length != 3) {
                        throw new IllegalArgumentException("Wrong file format! Expected 3 arguments, got - "
                                + inputData.length);
                    }

                    StringBuilder sb = new StringBuilder();
                    sb.append(inputData[0] + "|t|" + inputData[1] + "\n")
                      .append(inputData[0] + "|a|" + inputData[2] + "\n")
                      .append(lastReadLine[0] == null ? "" : convertAnnotationsString(lastReadLine[0]) + "\n");

                    try {
                        while (brAnns.ready() &&
                                (lastReadLine[0] = brAnns.readLine()).startsWith(inputData[0]))
                        {
                            sb.append( convertAnnotationsString(lastReadLine[0]) + "\n" );
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return sb.append("\n").toString();
                }).collect( Collectors.joining() );

                return result;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;*/
    }

    public static void main(String[] args) {
        new TsvToPubTatorConverter().convertAndSave(
                ResourceUtils.getResourceAsFile("textsets/training/trainingCEMP.txt"),
                ResourceUtils.getResourceAsFile("annotationsets/training/trainingCEMP.tsv"));
    }
}
