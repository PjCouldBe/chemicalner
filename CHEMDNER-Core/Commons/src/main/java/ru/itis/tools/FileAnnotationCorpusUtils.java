package ru.itis.tools;

import ru.itis.input.TextType;
import ru.itis.mark.*;
import ru.itis.mark.enums.C1Type;
import ru.itis.mark.enums.C2Type;
import ru.itis.utils.ResourceUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Filippov on 14.07.2016.
 */
public class FileAnnotationCorpusUtils {
    public static final String CEMP_TRAINING_ANNOTATIONS_PATH = "annotationsets/training/trainingCEMP.tsv";
    public static final String GPRO_TRAINING_ANNOTATIONS_PATH = "annotationsets/training/trainingGPRO.tsv";

    public static List<CEMPAnnotation> getTrainingAnnotations_CEMP() {
        return getCEMPAnnotationsFromFileResource(CEMP_TRAINING_ANNOTATIONS_PATH);
    }

    public static List<CEMPAnnotation> getDevelomentAnnotationSet_CEMP(String fileName) {
        return getCEMPAnnotationsFromFileResource("annotationsets/development/" + fileName);
    }

    public static List<CEMPAnnotation> getTestAnnotationSet_CEMP(String fileName) {
        return getCEMPAnnotationsFromFileResource("annotationsets/development/" + fileName);
    }

    public static List<CEMPAnnotation> getExternalAnnotationSet_CEMP(String absoluteFilePath) {
        return getCEMPAnnotationsFromFilePath(absoluteFilePath);
    }


    public static List<GPROAnnotation> getTrainingAnnotations_GPRO() {
        return getGPROAnnotationsFromFileResource(GPRO_TRAINING_ANNOTATIONS_PATH);
    }

    public static List<GPROAnnotation> getDevelomentAnnotationSet_GPRO(String fileName) {
        return getGPROAnnotationsFromFileResource("annotationsets/development/" + fileName);
    }

    public static List<GPROAnnotation> getTestAnnotationSet_GPRO(String fileName) {
        return getGPROAnnotationsFromFileResource("annotationsets/development/" + fileName);
    }

    public static List<GPROAnnotation> getExternalAnnotationSet_GPRO(String absoluteFilePath) {
        return getGPROAnnotationsFromFilePath(absoluteFilePath);
    }



    private static List<CEMPAnnotation> getCEMPAnnotationsFromFileResource(String path) {
        return getCEMPAnnotationsFromFilePath(ResourceUtils.getResourceAsPath(path));
    }
    private static List<CEMPAnnotation> getCEMPAnnotationsFromFilePath(String fullPath) {
        if (fullPath.endsWith(".tsv") == false && fullPath.endsWith(".txt") == false) {
            throw new IllegalArgumentException("File must be of .tsv or .txt format to handle it correctly");
        }

        try (BufferedReader br = new BufferedReader(
                                    new FileReader(fullPath)))
        {
            return br.lines().map(line -> {
                String[] args = line.split("\\t");
                if (args.length != 6) {
                    throw new RuntimeException("Wrong file format! Expected 5 arguments, got - " + args.length);
                }
                return new CEMPAnnotation(args[0],
                        TextType.valueOf(args[1]),
                        Integer.parseInt(args[2]),
                        Integer.parseInt(args[3]),
                        args[4],
                        CEMPAnnotation.CEMType.valueOf(args[5]));
            }).collect( Collectors.toList() );
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static List<GPROAnnotation> getGPROAnnotationsFromFileResource(String path) {
        return getGPROAnnotationsFromFilePath(ResourceUtils.getResourceAsPath(path));
    }
    private static List<GPROAnnotation> getGPROAnnotationsFromFilePath(String path) {
        if (path.endsWith(".tsv") == false && path.endsWith(".txt") == false) {
            throw new IllegalArgumentException("File must be of .tsv or .txt format to handle it correctly");
        }

        try (BufferedReader br = new BufferedReader(
                                    new FileReader(path)))
        {
            return br.lines().map(line -> {
                String[] args = line.split("\\t");
                if (args.length != 7) {
                    throw new RuntimeException("Wrong file format! Expected 7 arguments, got - " + args.length);
                }

                boolean isMentionOfType2 = args[6].equals("C2") || args[6].equals("GPRO_TYPE_2");
                return new GPROAnnotation(args[0],
                        TextType.valueOf(args[1]),
                        Integer.parseInt(args[2]),
                        Integer.parseInt(args[3]),
                        args[4],
                        (GPROType<?>)(isMentionOfType2
                                ? new GPROType_C2(C2Type.valueOf(args[5]))
                                : new GPROType_C1(args[6], C1Type.valueOf(args[5])))
                );
            }).collect( Collectors.toList() );
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
