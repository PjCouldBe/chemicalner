package ru.itis.tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Stream;

import ru.itis.mark.ChemicalAnnotation;

/**
 * Created by Dima on 13.07.2016.
 */
public class SimpleDictionaryBuilder {
    public File buildCEMPDictionary() {
        return buildDictionary("CEMPDictionary", FileAnnotationCorpusUtils.getTrainingAnnotations_CEMP());
    }

    public File buildGPRODictionary() {
        return buildDictionary("GPRODictionary", FileAnnotationCorpusUtils.getTrainingAnnotations_GPRO());
    }

    private <C extends ChemicalAnnotation> File buildDictionary(String dictionaryName, List<C> annotations) {
        File dictionaryFile = new File("src/main/resources/nlp/dictionaries/" + dictionaryName + ".txt");

        try (PrintWriter pw = new PrintWriter(
                new BufferedWriter(
                        new FileWriter(dictionaryFile))))
        {
            annotations.stream()
                       .map(ann -> ann.getOriginalText())
                       .forEach(txt -> pw.println( txt.trim() ));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return dictionaryFile;
    }

    public static void main(String[] args) {
        SimpleDictionaryBuilder dictBuilder = new SimpleDictionaryBuilder();
        File f;

        f = dictBuilder.buildCEMPDictionary();
        if (f == null) throw new RuntimeException(" CEMP dictionary was not builded! ");
        f = dictBuilder.buildGPRODictionary();
        if (f == null) throw new RuntimeException(" GPRO dictionary was not builded! ");
    }
}
