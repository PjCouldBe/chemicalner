package ru.itis.tools;

import ru.itis.input.ChemicalInput;
import ru.itis.input.TextType;
import ru.itis.mark.ChemicalAnnotation;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Dima on 31.07.2016.
 */
public class RawToGroupedAnnotationsConverter {
    public static <A extends ChemicalAnnotation> List<A[]> convert(Collection<ChemicalInput> rawTexts, List<A> rawAnnotations) {
        //long patentIdsCount = rawAnnotations.stream().map(ann -> ann.getPatentId()).distinct().count();
        Set<String> patentIdsDistinct = rawTexts.stream().map(t -> t.getPatentId()).collect(Collectors.toSet());

        Map<String, List<A>> groupedAnnotationsT = rawAnnotations.stream()
                .filter(e -> e.getTextType() == TextType.T)
                .collect(Collectors.groupingBy(e1 -> e1.getPatentId()));
        Map<String, List<A>> groupedAnnotationsA = rawAnnotations.stream()
                .filter(e -> e.getTextType() == TextType.A)
                .collect(Collectors.groupingBy(e1 -> e1.getPatentId()));

        List<A[]> groupedAnnotations = new ArrayList<>(rawTexts.size() * 2);
        addAllToList(groupedAnnotations, groupedAnnotationsT, patentIdsDistinct);
        addAllToList(groupedAnnotations, groupedAnnotationsA, patentIdsDistinct);

        return groupedAnnotations;
    }
    private static <A extends ChemicalAnnotation> List<A[]> addAllToList(
            List<A[]> dest, Map<String, List<A>> source, Set<String> patentIdsDistinct)
    {
        for (String patId : patentIdsDistinct) {
            A[] anns = source.containsKey(patId)
                    ? toArray(source.get(patId))
                    : (A[])new ChemicalAnnotation[0];
            dest.add( anns );
        }

        return dest;
    }
    private static <A extends ChemicalAnnotation> A[] toArray(List<A> lst) {
        A[] arr = (A[])new ChemicalAnnotation[lst.size()];
        return lst.toArray(arr);
    }

}
