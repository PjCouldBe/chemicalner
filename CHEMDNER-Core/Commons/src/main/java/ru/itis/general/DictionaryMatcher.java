package ru.itis.general;

import ru.itis.utils.ResourceUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Dima on 13.07.2016.
 */
public class DictionaryMatcher {
    private Set<String> dictionary;

    public DictionaryMatcher(String dictionaryPath) {
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader( ResourceUtils.getResource("nlp/dictionaries/" + dictionaryPath) ))) {
            this.dictionary = br.lines().collect( Collectors.toSet() );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean match(String token) {
        return dictionary.contains(token);
    }

    public boolean[] match(String[] tokens) {
        boolean[] res = new boolean[tokens.length];
        for (int i = 0; i < tokens.length; i++) {
            res[i] = match(tokens[i]);
        }
        return res;
    }
}
