package ru.itis.general;

import ru.itis.data.ChemicalOutputBatch;
import ru.itis.data.OutputBatch;
import ru.itis.input.ChemicalNormalizedInput;
import ru.itis.mark.ChemicalAnnotation;
import ru.itis.mark.ChemicalAnnotationType;
import ru.itis.model.IModelAlgorithm;import ru.itis.preprocess.FeatureSet;
import ru.itis.preprocess.FeatureSetTraining;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dima on 29.07.2016.
 */
public abstract class ChemicalModelAlgorithm<A extends ChemicalAnnotation>
        implements IModelAlgorithm<ChemicalNormalizedInput, A> {
    @Override
    public void learn(FeatureSetTraining<ChemicalNormalizedInput, A> featureSetTraining) {
        return;
    }

    @Override
    public <T extends OutputBatch<A>> T apply(FeatureSet fs) {
        if (fs.size() % 2 != 0) {
            throw new IllegalStateException(" Error during FeatureSet creation! Size cannot be odd! ");
        }
        int sz2 = fs.size() / 2;

        String[] patentIds = new String[sz2];
        String[] titles = new String[sz2];
        String[] abstracts = new String[sz2];
        List<ChemicalOutputBatch.ChemicalBatchNonTextData[]> batchList = new ArrayList<>(sz2);

        FeatureSet<ChemicalNormalizedInput>.FeatureSetBatch<ChemicalNormalizedInput>[] fsb = fs.toArray();
        for (int i = 0; i < sz2; i++) {
            patentIds[i] = fsb[i].getInputData().getPatentId();
            titles[i] = fsb[i].getInputData().getText();
            abstracts[i] = fsb[sz2 + i].getInputData().getText();
        }
        for (int i = 0; i < fs.size(); i++) {
            List<ChemicalOutputBatch.ChemicalBatchNonTextData> batch = new ArrayList<>();
            Token[] tokens = fsb[i].getSpecifiedParameter(Token[].class);
            boolean[] dictFiltered = fsb[i].getSpecifiedParameter(boolean[].class);

            for (int j = 0; j < tokens.length; j++) {
                if (dictFiltered[j]) {
                    Token t = tokens[j];
                    batch.add(
                            new ChemicalOutputBatch.ChemicalBatchNonTextData(
                                    t.getStart(), t.getLength(), produceAnnotationType(fsb[i].getInputData(), t) ));
                }
            }

            batchList.add( batch.toArray(new ChemicalOutputBatch.ChemicalBatchNonTextData[batch.size()]) );
        }

        return createOutputBatch(patentIds, titles, abstracts, batchList);
    }

    protected abstract <C extends ChemicalAnnotationType> C produceAnnotationType(
            ChemicalNormalizedInput input, Token t);

    protected abstract <T extends OutputBatch<A>> T createOutputBatch(
            String[] patentIDs, String[] titleTexts, String[] abstractTexts,
            List<ChemicalOutputBatch.ChemicalBatchNonTextData[]> batchData);
}
