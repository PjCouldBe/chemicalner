package ru.itis.general;

import ru.itis.input.ChemicalNormalizedInput;
import ru.itis.utils.ResourceUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Dima on 16.07.2016.
 */
public class DictionaryStraightMatcher {
    private Set<String> dictionary;

    public DictionaryStraightMatcher(String dictionaryPath) {
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader( ResourceUtils.getResource("nlp/dictionaries/" + dictionaryPath) ))) {
            this.dictionary = br.lines().collect( Collectors.toSet() );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Token> match(ChemicalNormalizedInput input) {
        List<Token> tokenList = new ArrayList<>();

        for (String dictEntry : dictionary) {
            Matcher m = Pattern.compile(dictEntry, Pattern.LITERAL).matcher(input.getText());

            while (m.find()) {
                tokenList.add( new Token(m.group(), m.start(), (byte)(m.end() - m.start())) );
            }
        }

        Collections.sort(tokenList, Comparator.comparing(Token::getStart).thenComparing(Token::getLength));
        return tokenList;
    }
}
