package ru.itis.general;

import ru.itis.preprocess.NLPPipeline;
import ru.itis.input.ChemicalNormalizedInput;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Dima on 13.07.2016.
 */
public class BaselineNLPPipeline extends NLPPipeline<ChemicalNormalizedInput> {
    public BaselineNLPPipeline(String dictionaryPath) {
        super(ChemicalNormalizedInput.class);

        addHandler(
            lists -> {
                Tokenizer tokenizer = new Tokenizer();
                ArrayList<Token[]> result = new ArrayList<>();
                for (ChemicalNormalizedInput in : (List<ChemicalNormalizedInput>)lists[0]) {
                    result.add( tokenizer.tokenize(in.getText()) );
                }

                return result;
            },
            Token[].class,
            new Class[]{ChemicalNormalizedInput.class}
        );

        addHandler(
            lists -> {
                DictionaryMatcher dict = new DictionaryMatcher(dictionaryPath);
                ArrayList<boolean[]> result = new ArrayList<>();

                for (Object obj : (List<Token[]>)lists[0]) {
                    String[] txts = Arrays.stream((Token[])obj).map(t -> t.getText()).toArray(String[]::new);
                    result.add( dict.match(txts) );
                }

                return result;
            },
            boolean[].class,
            new Class[] {Token[].class}
        );
    }
}
