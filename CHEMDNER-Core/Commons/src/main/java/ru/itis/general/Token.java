package ru.itis.general;

/**
 * Created by Dima on 16.07.2016.
 */
public class Token {
    private String text;
    private int start;
    private byte length;

    public Token(String text, int start, byte length) {
        this.text = text;
        this.start = start;
        this.length = length;
    }

    public String getText() {
        return text;
    }

    public int getStart() {
        return start;
    }

    public byte getLength() {
        return length;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public void setLength(byte length) {
        this.length = length;
    }
}
