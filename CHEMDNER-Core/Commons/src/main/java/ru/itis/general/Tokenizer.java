package ru.itis.general;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Dima on 13.07.2016.
 */
public class Tokenizer {
    final String tokenizationPattern = "[^\\p{Cntrl}\\r\\t\\n\\v !~`|{}+*:;^$%&?@<=>]+";

    public Token[] tokenize(String text) {
        Matcher textMatcher = Pattern.compile(tokenizationPattern).matcher(text);
        List<Token> tokenList = new ArrayList<>( text.length() / 7 );  //At once we assign capacity of
                                                                       //supposed count of words in the text.
                                                                       //So, text.length / avg word length (== 7)

        while (textMatcher.find()) {
            tokenList.add( new Token(textMatcher.group(),
                                     textMatcher.start(),
                                    (byte)(textMatcher.end() - textMatcher.start())) );
        }

        return tokenList.toArray(new Token[0]);
    }

    /*public String[] tokenize(String text) {
        return Stream.of( text.split(tokenizationPattern) )
                     .filter(s -> ! s.isEmpty() )
                     .toArray( String[]::new );
    }*/
}
