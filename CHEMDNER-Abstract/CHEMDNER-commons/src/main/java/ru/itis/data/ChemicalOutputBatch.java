package ru.itis.data;

import ru.itis.input.TextType;
import ru.itis.mark.ChemicalAnnotation;
import ru.itis.mark.ChemicalAnnotationType;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by Dima on 29.07.2016.
 */
public abstract class ChemicalOutputBatch<A extends ChemicalAnnotation> extends OutputBatch<A> {
    public static class ChemicalBatchNonTextData {
        private int start;
        private byte len;
        private ChemicalAnnotationType annType;

        public ChemicalBatchNonTextData(int start,
                                        byte len,
                                        @Nonnull ChemicalAnnotationType annType) {
            this.start = start;
            this.len = len;
            this.annType = annType;
        }

        public int getStart() {
            return start;
        }

        public int getLen() {
            return len;
        }

        public ChemicalAnnotationType getAnnType() {
            return annType;
        }
    }

    private String patentString;
    private String wholeTextString;
    private int[] starts;
    private byte[] lens;
    private int[] annotationAmounts;
    private ArrayList<ChemicalAnnotationType> distinctTypes = new ArrayList<>(20);
    private byte[] annotationTypeIds;

    private int lastCumulatedCount = 0;  //first index of last taken annotation group number in numeric data arrays
    private int lastI = 0;  //last taken annotations group number

    public ChemicalOutputBatch(@Nonnull String[] patentIDs,
                               @Nonnull String[] titleTexts,
                               @Nonnull String[] abstractTexts,
                               @Nonnull List<ChemicalBatchNonTextData[]> batchData)
    {
        if ( ! checkArgs(patentIDs, titleTexts, abstractTexts, batchData) ) {
            throw new IllegalArgumentException(" " +
                    "Provided arguments represent not the same amount of output annotations! ");
        }

        String tab = "\t".intern();
        this.patentString = String.join(tab, patentIDs);
        this.wholeTextString = String.join(tab, titleTexts) + tab + String.join(tab, abstractTexts);

        annotationAmounts = new int[batchData.size()];
        int size = batchData.stream().mapToInt(arr -> arr.length).sum();
        starts = new int[size];
        lens = new byte[size];
        annotationTypeIds = new byte[size];

        int lastAnnotationIndex = 0;
        for (int i = 0; i < batchData.size(); i++) {
            ChemicalBatchNonTextData[] data = batchData.get(i);
            annotationAmounts[i] = data.length;

            for (int j = lastAnnotationIndex, jData = 0; j < lastAnnotationIndex + data.length; j++, jData++) {
                starts[j] = data[jData].start;
                lens[j] = data[jData].len;

                int indOf = distinctTypes.indexOf(data[jData].annType);
                if (indOf == -1) {
                    indOf = distinctTypes.size();
                    distinctTypes.add(data[jData].annType);
                }
                annotationTypeIds[j] = (byte)indOf;
            }

            lastAnnotationIndex += data.length;
        }
    }
    private boolean checkArgs(String[] patentIDs,
                              String[] titleTexts,
                              String[] abstractTexts,
                              List<ChemicalBatchNonTextData[]> batchData)
    {
        int patentStringLen = patentIDs.length;

        if (patentStringLen != titleTexts.length
                || patentStringLen != abstractTexts.length
                || patentStringLen != batchData.size() / 2
                || batchData.size() % 2 != 0) {
            return false;
        }
        return true;
    }

    @Override
    public List<A> getAnnotation(int i) {
        catchI(i);

        String[] patentIds = patentString.split("\t");

        String patentId = patentIds[ (i >= patentIds.length) ? i - patentIds.length : i ];
        String text = wholeTextString.split("\t")[i];

        List<A> result = new ArrayList<>(annotationAmounts[i]);
        for (int j = lastCumulatedCount; j < lastCumulatedCount + annotationAmounts[i]; j++) {
            TextType t = (j >= patentIds.length) ? TextType.T : TextType.A;
            int start = starts[j];
            int end = start + lens[j];
            String annText = text.substring(start, end);
            ChemicalAnnotationType type = distinctTypes.get( annotationTypeIds[j] );

            result.add( getAnnotation(patentId, t, start, end, annText, type) );
        }

        return result;
    }
    private void catchI(int i) {
        if (i < 0 || i > annotationAmounts.length) throw new IllegalArgumentException();

        if (lastI < i) {
            for (; lastI < i; lastI++) {
                lastCumulatedCount += annotationAmounts[lastI];
            }
        } else {
            for (; lastI > i; lastI--) {
                lastCumulatedCount -= annotationAmounts[lastI];
            }
        }
    }

    @Override
    public List<A[]> getGroupedAnnotations(Function<Supplier<?>[], A[]> function) {
        String[] patentIds = patentString.split("\t");
        String[] texts = wholeTextString.split("\t");

        StringBuilder sb;
        String patentId;
        TextType t;
        int start;
        int end;
        String annText;
        ChemicalAnnotationType type;

        List<A[]> result = new ArrayList<>(starts.length);
        A[] annotaionGroup;
        int pIdsLength = patentIds.length;
        int lastAnnotationIndex = 0;
        for (int i = 0; i < texts.length; i++) {
            if (i < pIdsLength) {
                patentId = patentIds[i];
                t = TextType.T;
            } else {
                patentId = patentIds[i - pIdsLength];
                t = TextType.A;
            }

            sb = new StringBuilder(texts[i]);
            annotaionGroup = (A[])new ChemicalAnnotation[annotationAmounts[i]];
            for (int j = lastAnnotationIndex; j < lastAnnotationIndex + annotationAmounts[i]; j++) {
                start = starts[j];
                end = start + lens[j];
                annText = sb.substring(start, end);
                type = distinctTypes.get( annotationTypeIds[j] );

                annotaionGroup[j - lastAnnotationIndex] = getAnnotation(patentId, t, start, end, annText, type);
            }
            lastAnnotationIndex += annotationAmounts[i];
            result.add( annotaionGroup );
        }

        return result;
    }

    @Override
    public List<A> getOutputAnnotations() {
        String[] patentIds = patentString.split("\t");
        String[] texts = wholeTextString.split("\t");

        StringBuilder sb;
        String patentId;
        TextType t;
        int start;
        int end;
        String annText;
        ChemicalAnnotationType type;

        List<A> result = new ArrayList<>(starts.length);
        int pIdsLength = patentIds.length;
        int lastAnnotationIndex = 0;
        for (int i = 0; i < texts.length; i++) {
            if (i < pIdsLength) {
                patentId = patentIds[i];
                t = TextType.T;
            } else {
                patentId = patentIds[i - pIdsLength];
                t = TextType.A;
            }

            sb = new StringBuilder(texts[i]);
            for (int j = lastAnnotationIndex; j < lastAnnotationIndex + annotationAmounts[i]; j++) {
                start = starts[j];
                end = start + lens[j];
                annText = sb.substring(start, end);
                type = distinctTypes.get( annotationTypeIds[j] );

                result.add( getAnnotation(patentId, t, start, end, annText, type) );
            }
            lastAnnotationIndex += annotationAmounts[i];
        }

        return result;
    }

    protected abstract <T extends ChemicalAnnotationType> A getAnnotation(
            String patentId, TextType type, int start, int end, String text, T annType);
}
