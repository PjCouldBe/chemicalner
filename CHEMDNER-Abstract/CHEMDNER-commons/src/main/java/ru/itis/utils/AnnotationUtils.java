package ru.itis.utils;

import ru.itis.mark.ChemicalAnnotation;

/**
 * Created by Dima on 13.07.2016.
 */
public class AnnotationUtils {
    public static int getTextShift(ChemicalAnnotation ann) {
        return ann.toShortString().length() - (ann.getEnd() - ann.getStart());
    }
}
