package ru.itis.evaluation;

import org.apache.commons.lang.RandomStringUtils;
import ru.itis.mark.ChemicalAnnotation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Dima on 05.08.2016.
 */
public class ChemicalAnnotationToEvalPathConverter<C extends ChemicalAnnotation> {
   public List<String> convert(List<C> list) {
       try {
           File f = File.createTempFile(RandomStringUtils.random(10, true, true), ".tsv");
           f.deleteOnExit();

           try (PrintWriter pw = new PrintWriter(new FileWriter(f))) {
               convertAndToStringList(list).stream().forEachOrdered(line -> pw.println(line));
           }

           List<String> res = new ArrayList<>();
           res.add(f.getAbsolutePath());
           return res;
       } catch (IOException e) {
           e.printStackTrace();
           return null;
       }
   }

    private List<String> convertAndToStringList(List<C> annotations) {
        Comparator<C> comp = Comparator.comparing(C::getTextType)
                .thenComparingInt(C::getStart).thenComparingInt(C::getEnd);
        Function<List<C>, List<String>> transformer = lst -> {
            List<String> result = new ArrayList<>(lst.size());
            int i = 1;
            for (ChemicalAnnotation ann : lst) {
                result.add(ann.getPatentId() + "\t" +
                        ann.getTextType() + ":" + ann.getStart() + ":" + ann.getEnd() + "\t" +
                        (i++) + "\t" +
                        0.99 + "\t" +
                        ann.getOriginalText());
            }
            return result;
        };

        Stream<List<String>> listStream = annotations.stream()
                .collect(Collectors.groupingBy(ann -> ann.getPatentId()))
                .entrySet()
                .parallelStream()
                .map(entry -> {
                    List<C> valueList = entry.getValue();
                    Collections.sort(valueList, comp);
                    return valueList;
                }).map(transformer);
        return listStream.flatMap(List::stream).collect(Collectors.toList());
    }
}
