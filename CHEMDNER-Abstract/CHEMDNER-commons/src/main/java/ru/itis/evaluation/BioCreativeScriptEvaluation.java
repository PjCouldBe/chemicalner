package ru.itis.evaluation;

import gnu.trove.map.hash.TObjectIntHashMap;
import ru.itis.evaluate.Evaluation;
import ru.itis.evaluate.ScriptEvaluationMethod;
import ru.itis.mark.Actual;
import ru.itis.mark.Expected;
import ru.itis.utils.ResourceUtils;

import javax.annotation.Nonnull;

/**
 * Created by Dima on 05.08.2016.
 */
public class BioCreativeScriptEvaluation implements ScriptEvaluationMethod {
    @Override
    public Evaluation evaluate(@Nonnull Expected<String> expected, @Nonnull Actual<String> actual) {
        String evalResult = new CMDScriptTool().launchScript(
                ResourceUtils.getResourceAsPath("instruments/bc_evaluation-3.2/bc-evaluate.py"),
                "--INT",
                actual.get().get(0),
                expected.get().get(0)
        );

        TObjectIntHashMap<String> name2place = new TObjectIntHashMap<>(16);
        int[] intEvals = new int[3];
        double[] doubleEvals = new double[13];
        Object[] objEvals = new Object[0];
        int intCount = 0, doubleCount = 0;

        String pattern = "\\s+";
        String[] components = evalResult.split(pattern);
        String lastPrefix = "";
        boolean colonFound = false;
        String chName = "";
        boolean skip = true;
        boolean addAvrg = false;
        for (String c : components) {
            if (skip) {
                if (c.equals("Hits")) skip = false;
                continue;
            } else if (isValidPrefix(c)) {
                lastPrefix = c;
            } else if (c.equals("Avrg")) {
                addAvrg = true;
            } else if (c.endsWith(":")) {
                colonFound = true;
                chName = (lastPrefix.isEmpty() ? "" : lastPrefix + " ")
                                + (addAvrg ? "Average " : "")
                                + resolveAAcronym(c.substring(0, c.length()-1));
                addAvrg = false;
            } else if (colonFound) {
                colonFound = false;
                try {
                    double d = Double.parseDouble(c);
                    if (lastPrefix.isEmpty()) {
                        intEvals[intCount] = (int)d;
                        name2place.put(chName, 100 + intCount);
                        intCount++;
                    } else {
                        doubleEvals[doubleCount] = d;
                        name2place.put(chName, 200 + doubleCount);
                        doubleCount++;
                    }
                } catch (NumberFormatException e) {
                    //skip it
                }
            }
        }

        return new Evaluation(name2place, intEvals, doubleEvals, objEvals);
    }
    private boolean isValidPrefix(String str) {
        String s = str.toLowerCase();
        return s.equals("macro") || s.equals("micro") || s.equals("stddev");
    }
    private String resolveAAcronym(String s) {
        if (s.contains("precs.") || s.equals("P")) {
            return s.replace("precs.", "precision").replace("P", "Precision");
        } else if (s.contains("-scr.") || s.contains("-s.")) {
            return s.replace("-scr.", "-score").replace("-s.", "-score");
        } else if (s.contains("recall") || s.equals("TP") || s.equals("FP") || s.equals("FN")) {
            return s;
        } else {
            return null;
        }
    }
}
