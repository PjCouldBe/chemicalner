package ru.itis.mark;

import ru.itis.input.TextType;

import java.util.Objects;

/**
 * Created by Dima on 09.07.2016.
 */
public abstract class ChemicalAnnotation extends Annotation {
    protected TextType type;
    protected String patentId;

    public ChemicalAnnotation(String patentId, TextType t, int start, int end, String text) {
        super(start, end, text);
        this.patentId = Objects.requireNonNull(patentId);
        this.type = Objects.requireNonNull(t);
    }

    public String getPatentId() {
        return patentId;
    }

    public void setPatentId(String patentId) {
        this.patentId = patentId;
    }

    public TextType getTextType() {
        return type;
    }

    public void setTextType(TextType t) {
        type = t;
    }

    public abstract ChemicalAnnotationType getType();

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(patentId).append("\t")
          .append(type).append("\t")
          .append(start).append("\t")
          .append(end).append("\t")
          .append(text);
        return sb.toString();
    }

    public String toShortString() {
        return "[" + getType().toString() + ":  {" + text + "}]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChemicalAnnotation)) return false;
        if (!super.equals(o)) return false;

        ChemicalAnnotation that = (ChemicalAnnotation) o;

        if (type != that.type) return false;
        return patentId.equals(that.patentId);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + patentId.hashCode();
        return result;
    }
}
