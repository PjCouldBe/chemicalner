package ru.itis.input;

import ru.itis.data.AbstractInput;
import ru.itis.data.NormalizeInputRule;

import javax.annotation.Nonnull;

/**
 * Created by Dima on 13.07.2016.
 */
public class ChemicalInput implements AbstractInput {
    private String patentId;
    private String titleText;
    private String abstractText;

    public static final NormalizeInputRule<ChemicalInput, ChemicalNormalizedInput> NORMALIZE_BY_TITLE_RULE =
            input -> new ChemicalNormalizedInput( input.patentId, TextType.T, input.titleText );
    public static final NormalizeInputRule<ChemicalInput, ChemicalNormalizedInput> NORMALIZE_BY_ABSTRACT_RULE =
            input -> new ChemicalNormalizedInput( input.patentId, TextType.T, input.abstractText );

    public ChemicalInput(@Nonnull String patentId,
                         @Nonnull String titleText,
                         @Nonnull String abstractText)
    {
        this.patentId = patentId;
        this.titleText = titleText;
        this.abstractText = abstractText;
    }

    public String getPatentId() {
        return patentId;
    }

    public String getTitleText() {
        return titleText;
    }

    public String getAbstractText() {
        return abstractText;
    }
}
