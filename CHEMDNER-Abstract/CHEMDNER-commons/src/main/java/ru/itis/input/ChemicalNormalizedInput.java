package ru.itis.input;

import ru.itis.data.NormalizedInput;

import javax.annotation.Nonnull;

/**
 * Created by Dima on 13.07.2016.
 */
public class ChemicalNormalizedInput implements NormalizedInput {
    private String patentId;
    private TextType textType;
    private String text;

    public ChemicalNormalizedInput(@Nonnull String patentId,
                                   @Nonnull TextType textType,
                                   @Nonnull String text) {
        this.patentId = patentId;
        this.textType = textType;
        this.text = text;
    }

    public String getPatentId() {
        return patentId;
    }

    public TextType getTextType() {
        return textType;
    }

    public String getText() {
        return text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChemicalNormalizedInput that = (ChemicalNormalizedInput) o;

        if (!patentId.equals(that.patentId)) return false;
        if (textType != that.textType) return false;
        return text.equals(that.text);
    }

    @Override
    public int hashCode() {
        int result = patentId.hashCode();
        result = 31 * result + textType.hashCode();
        result = 31 * result + text.hashCode();
        return result;
    }
}
