package ru.itis.input;

import ru.itis.data.Input;

import javax.annotation.Nonnull;
import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Dima on 13.07.2016.
 */
public class InputProducer {
    public static Input<ChemicalNormalizedInput> produce(@Nonnull File file) {
        if (file.getName().endsWith(".tsv") == false && file.getName().endsWith(".txt") == false) {
            throw new IllegalArgumentException("File must be of .tsv or .txt format to handle it correctly");
        }

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            return produce(br);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Input<ChemicalNormalizedInput> produce(@Nonnull InputStream out) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(out))) {
            return produce(br);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static Input<ChemicalNormalizedInput> produce(@Nonnull BufferedReader reader) {
        List<ChemicalInput> lst =
                reader.lines().map(line -> {
                   String[] args = line.split("\\t");
                   if (args.length != 3) {
                       throw new RuntimeException("Wrong file format! Expected 3 arguments, got - " + args.length);
                   }
                   return new ChemicalInput(args[0], args[1], args[2]);
               }).collect( Collectors.toList() );

        Input input = new Input<>(lst, ChemicalInput.NORMALIZE_BY_TITLE_RULE);
        input.addInput(lst, ChemicalInput.NORMALIZE_BY_ABSTRACT_RULE);
        return input;
    }
}
