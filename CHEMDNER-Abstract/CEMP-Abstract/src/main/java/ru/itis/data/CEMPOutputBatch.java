package ru.itis.data;

import ru.itis.input.TextType;
import ru.itis.mark.CEMPAnnotation;
import ru.itis.mark.ChemicalAnnotationType;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Created by Dima on 29.07.2016.
 */
public class CEMPOutputBatch extends ChemicalOutputBatch<CEMPAnnotation> {
    public CEMPOutputBatch(@Nonnull String[] patentIDs,
                           @Nonnull String[] titleTexts,
                           @Nonnull String[] abstractTexts,
                           @Nonnull List<ChemicalBatchNonTextData[]> batchData) {
        super(patentIDs, titleTexts, abstractTexts, batchData);
    }

    @Override
    protected <T extends ChemicalAnnotationType> CEMPAnnotation getAnnotation(
            String patentId, TextType type, int start, int end, String text, T annType) {
        return new CEMPAnnotation(patentId, type, start, end, text, (CEMPAnnotation.CEMType)annType);
    }
}
