package ru.itis.mark;

import ru.itis.input.TextType;

import java.util.Objects;

/**
 * Created by Dima on 09.07.2016.
 */
public class CEMPAnnotation extends ChemicalAnnotation {
    public enum CEMType implements ChemicalAnnotationType {
        SYSTEMATIC,      //IUPAC and IUPAC-like names
        IDENTIFIER,      //CAS, PubCHEM, ChEBI, CID, CHEMBL and others' ids
        FORMULA,         //Molekular formula, SMILES, InChl, InChiKey
        TRIVIAL,         //Trivial, habitual names
        ABBREVIATION,    //Acronyms of chemcials compounds
        FAMILY,          //Generalized ids of all above mention types (likes alkyl or ROH)
        MULTIPLE         //Complex and sequenced names of chemicals
    }
    protected CEMType cemType;

    public CEMPAnnotation(String patentId, TextType t, int start, int end, String text) {
        super(patentId, t, start, end, text);
    }

    public CEMPAnnotation(String patentId, TextType t, int start, int end, String text, CEMType cemType) {
        super(patentId, t, start, end, text);
        this.cemType = Objects.requireNonNull(cemType);
    }

    @Override
    public ChemicalAnnotationType getType() {
        return cemType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CEMPAnnotation)) return false;
        if (!super.equals(o)) return false;

        CEMPAnnotation that = (CEMPAnnotation) o;

        return cemType == that.cemType;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + cemType.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return super.toString() + "\t" + cemType.toString();
    }
}
