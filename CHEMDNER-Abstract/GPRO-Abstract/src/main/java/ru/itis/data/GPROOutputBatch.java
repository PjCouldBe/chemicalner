package ru.itis.data;

import ru.itis.input.TextType;
import ru.itis.mark.ChemicalAnnotationType;
import ru.itis.mark.GPROAnnotation;
import ru.itis.mark.GPROType;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Created by Dima on 29.07.2016.
 */
public class GPROOutputBatch extends ChemicalOutputBatch<GPROAnnotation> {
    public GPROOutputBatch(@Nonnull String[] patentIDs,
                           @Nonnull String[] titleTexts,
                           @Nonnull String[] abstractTexts,
                           @Nonnull List<ChemicalBatchNonTextData[]> batchData) {
        super(patentIDs, titleTexts, abstractTexts, batchData);
    }

    @Override
    protected <T extends ChemicalAnnotationType> GPROAnnotation getAnnotation(
            String patentId, TextType type, int start, int end, String text, T annType) {
        return new GPROAnnotation(patentId, type, start, end, text, (GPROType<?>)annType);
    }
}
