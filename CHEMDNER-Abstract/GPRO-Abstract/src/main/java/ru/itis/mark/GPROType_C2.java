package ru.itis.mark;

import ru.itis.mark.enums.C2Type;

/**
 * Created by Dima on 09.07.2016.
 */
public class GPROType_C2 extends GPROType<C2Type> {
    public GPROType_C2(C2Type entityClass) {
        super("C2", entityClass);
    }

    @Override
    public String getMentionType() {
        return mentionType;
    }

    @Override
    public String toString() {
        return entityClass.toString() + "\t" + "GPRO_TYPE_2";
    }
}