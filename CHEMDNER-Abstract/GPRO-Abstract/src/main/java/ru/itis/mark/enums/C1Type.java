package ru.itis.mark.enums;

/**
 * Created by Dima on 09.07.2016.
 */
public enum C1Type implements  GPROClass {
    NO_CLASS,
    SEQUENCE,
    FAMILY,
    MULTIPLE,
    NESTED,
    ABBREVIATION,
    IDENTIFIER,
    FULL_NAME;
}
