package ru.itis.mark;

import ru.itis.mark.enums.C1Type;

/**
 * Created by Dima on 09.07.2016.
 */
public class GPROType_C1 extends GPROType<C1Type> {
    public GPROType_C1(C1Type entityClass) {
        super("C1", entityClass);
    }

    public GPROType_C1(String databaseId, C1Type entityClass) {
        super(databaseId, entityClass);
    }

    @Override
    public String getMentionType() {
        return "C1" + (mentionType.equals("C1") ? "" : " " + mentionType);
    }

    @Override
    public String toString() {
        return entityClass.toString() + "\t" + mentionType;
    }
}
