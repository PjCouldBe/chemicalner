package ru.itis.mark;

import ru.itis.input.TextType;
import ru.itis.mark.enums.GPROClass;

import javax.annotation.Nonnull;

/**
 * Created by Dima on 09.07.2016.
 */
public class GPROAnnotation extends ChemicalAnnotation {
    protected GPROType<? extends GPROClass> gproType;

    public GPROAnnotation(String patentId, TextType t, int start, int end, String text) {
        super(patentId, t, start, end, text);
    }

    public <T extends GPROClass> GPROAnnotation(
            String patentId, TextType t, int start, int end, String text, @Nonnull GPROType<T> type)
    {
        super(patentId, t, start, end, text);
        this.gproType = type;
    }

    @Override
    public ChemicalAnnotationType getType() {
        return gproType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GPROAnnotation)) return false;
        if (!super.equals(o)) return false;

        GPROAnnotation that = (GPROAnnotation) o;

        return gproType.equals(that.gproType);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + gproType.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return super.toString() + "\t" + gproType.toString();
    }
}