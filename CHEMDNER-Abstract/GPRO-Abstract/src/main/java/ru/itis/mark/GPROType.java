package ru.itis.mark;

import ru.itis.mark.enums.GPROClass;

/**
 * Created by Dima on 09.07.2016.
 */
public abstract class GPROType<T extends GPROClass> implements ChemicalAnnotationType {
    protected String mentionType;       //mention of type 1 or type 2;
                                        //if 1-st then the database normalized id should be here
                                        //else "C2" or "GPRO_TYPE_2" must be here
    protected T entityClass;

    protected GPROType(String mentionType, T entityClass) {
        this.mentionType = mentionType;
        this.entityClass = entityClass;
    }

    public abstract String getMentionType();

    public T getEntityClass() {
        return entityClass;
    }

    public void setEntityClass(T entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public String toString() {
        return getMentionType() + ":" + entityClass.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GPROType<?> gproType = (GPROType<?>) o;

        if (!mentionType.equals(gproType.mentionType)) return false;
        return entityClass.equals(gproType.entityClass);

    }

    @Override
    public int hashCode() {
        int result = mentionType.hashCode();
        result = 31 * result + entityClass.hashCode();
        return result;
    }
}