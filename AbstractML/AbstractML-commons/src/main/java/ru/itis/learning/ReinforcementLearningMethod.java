package ru.itis.learning;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import org.javatuples.Pair;
import ru.itis.data.NormalizedInput;
import ru.itis.learning.values.Resolution;
import ru.itis.learning.values.Situation;

/**
 * Created by Dima on 27.07.2016.
 */
public class ReinforcementLearningMethod<N extends NormalizedInput>
        extends ILearningMethod<N, Double> {
    @Override
    public List<Double[]> transformReactions(List<Double[]> trainingReactions) {
        if (trainingReactions == null || trainingReactions.isEmpty() || trainingReactions.size() != inputs.size()) {
            throw new IllegalArgumentException(""); //TODO
        }

        for (Double[] outs : trainingReactions) {
            if (outs == null || outs.length == 0) throw new IllegalArgumentException(""); //TODO
        }

        return trainingReactions;
    }
}
