package ru.itis.learning;

import ru.itis.data.NormalizedInput;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

/**
 * Created by Dima on 27.07.2016.
 */
public abstract class ILearningMethod<N extends NormalizedInput, OUT> {
    protected List<N> inputs;
    protected List<OUT[]> trainingReactions;

    public void setData(@Nonnull List<N> inputs,
                        List<OUT[]> trainingReactions) {
        this.inputs = inputs;
        this.trainingReactions = transformReactions(trainingReactions);
    }

    public abstract List<OUT[]> transformReactions(List<OUT[]> trainingReactions);

    public void setInputs(@Nonnull List<N> inputs) {
        if (inputs.size() != trainingReactions.size()) {
            throw new IllegalArgumentException(" There was insufficient training annotations set provided! ");
        }
        this.inputs = inputs;
    }

    public List<N> getInputs() {
        return inputs;
    }

    public List<OUT[]> getTrainingAnnotations() {
        return trainingReactions;
    }
}
