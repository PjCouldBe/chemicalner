package ru.itis.learning;

import ru.itis.data.NormalizedInput;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by Dima on 27.07.2016.
 */
public class SemisupervisedLearningMethod<N extends NormalizedInput, OUT> extends ILearningMethod<N, OUT> {
    @Override
    public List<OUT[]> transformReactions(List<OUT[]> trainingReactions) {
        if (trainingReactions == null || trainingReactions.isEmpty()) {
            throw new IllegalArgumentException(""); //TODO
        }

        boolean hasNulls = (trainingReactions.size() < inputs.size());
        boolean hasNonNulls = false;
        for (OUT[] reacts : trainingReactions) {
            if (reacts == null || reacts.length == 0) {
                hasNulls = true;
            } else {
                hasNonNulls = true;
            }

            if (hasNulls && hasNonNulls) return trainingReactions;
        }

        if (hasNonNulls == false) {
            throw new IllegalArgumentException(""); //TODO
        } else if (hasNulls == false) {
            System.err.println(" WARN: The semi-superviesd approach supposes for " +
                    "partially nulled values in trainning set reactions! ");
        }
        return trainingReactions;
    }
}
