package ru.itis.learning;

import ru.itis.data.NormalizedInput;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by Dima on 27.07.2016.
 */
public class UnsupervisedLearningMethod<N extends NormalizedInput> extends ILearningMethod<N, Void> {
    @Override
    public List<Void[]> transformReactions(List<Void[]> trainingReactions) {
        List<Void[]> res = new ArrayList<>(1);
        res.add(null);
        return res;
    }
}
