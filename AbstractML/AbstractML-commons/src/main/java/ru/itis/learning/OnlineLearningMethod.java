package ru.itis.learning;

import ru.itis.data.NormalizedInput;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by Dima on 27.07.2016.
 */
public class OnlineLearningMethod<N extends NormalizedInput, OUT> extends ILearningMethod<N, OUT> {
    @Override
    public List<OUT[]> transformReactions(List<OUT[]> trainingReactions) {
        if (trainingReactions == null || trainingReactions.isEmpty() || trainingReactions.size() != inputs.size()) {
            throw new IllegalArgumentException(""); //TODO
        }

        for (OUT[] outs : trainingReactions) {
            if (outs == null || outs.length == 0) throw new IllegalArgumentException(""); //TODO
        }

        return trainingReactions;
    }
}
