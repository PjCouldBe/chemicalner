package ru.itis.data;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Dima on 11.07.2016.
 */
public class InputNormalizer {
    private static final int THRESHOLD_SIZE = 100000;

    public static <RAW_IN extends AbstractInput, NORM_IN extends NormalizedInput> NORM_IN normalize(
            NormalizeInputRule<RAW_IN, NORM_IN> rule, RAW_IN in) {
        return rule.apply(in);
    }

    public static <RAW_IN extends AbstractInput, NORM_IN extends NormalizedInput> List<NORM_IN> normalize(
            @Nonnull NormalizeInputRule<RAW_IN, NORM_IN> rule, @Nonnull Collection<RAW_IN> inputs) {
        if (inputs.size() <= THRESHOLD_SIZE) {
            return inputs.stream().map(rule).collect( Collectors.toList() );
        } else {
            return inputs.parallelStream().map(rule).collect( Collectors.toList() );
        }
    }

    static <I extends AbstractInput,
            NORM_IN extends NormalizedInput> List<NORM_IN> normalizeUnsafe(
                        @Nonnull NormalizeInputRule<? extends AbstractInput, NORM_IN> rule,
                        @Nonnull Collection<? extends AbstractInput> inputs)
    {
        return normalize((NormalizeInputRule<I, NORM_IN>)rule, (Collection<I>)inputs);
    }
}
