package ru.itis.data;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by Filippov on 28.07.2016.
 */
public abstract class OutputBatch<OUT> {
    public abstract List<OUT> getAnnotation(int i);

    public abstract List<OUT[]> getGroupedAnnotations(Function<Supplier<?>[], OUT[]> classfier);

    public abstract List<OUT> getOutputAnnotations();
}
