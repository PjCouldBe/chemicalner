package ru.itis.data;

import org.apache.commons.collections.ListUtils;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * Created by Dima on 23.07.2016.
 */
public class Input<NORM_IN extends NormalizedInput> {
    final ArrayList<InputRulePair<?>> pairsList = new ArrayList<>();

    private class InputRulePair<I extends AbstractInput> extends AbstractMap.SimpleEntry<Collection<I>, NormalizeInputRule<I, NORM_IN>> {
        InputRulePair(Collection<I> value0, NormalizeInputRule<I, NORM_IN> value1) {
            super(value0, value1);
        }
    }

    public class InputBuilder<NORM_IN extends NormalizedInput> {
        Input input;

        public InputBuilder(Input srcInput) {
            this.input = srcInput;
        }

        private <T extends AbstractInput> int findByInputClass(Class<T> inputClass) {
            for (int index = 0; index < pairsList.size(); index++) {
                if (pairsList.get(index).getKey().iterator().next().getClass() == inputClass) {
                    return index;
                }
            }

            return -1;
        }

        public <T extends AbstractInput> InputBuilder remove(@Nonnull Class<T> inputClass) {
            int index = findByInputClass(inputClass);
            pairsList.remove(index);
            return this;
        }

        public <T extends AbstractInput> Collection<T> getInputCollection(@Nonnull Class<T> inputClass) {
            int index = findByInputClass(inputClass);
            return (Collection<T>)pairsList.get(index).getKey();
        }

       /* public <T extends AbstractInput, T_NEW extends  AbstractInput> InputBuilder replace(
                                                            @Nonnull Class<T> inputClass,
                                                            @Nonnull Collection<T_NEW> inputs,
                                                            @Nonnull NormalizeInputRule<T_NEW, NORM_IN> rule) {
            if (inputs.size() == 0) throw new IllegalArgumentException(" Input collection must not be empty! ");

            int index = findByInputClass( inputClass );
            new InputRulePair<T_NEW>(inputs, rule);
            pairsList.set(index, new InputRulePair<T_NEW>(inputs, rule));
            return this;
        }

        public <T extends AbstractInput, T_NEW extends  AbstractInput> InputBuilder replaceWithSameType(
                @Nonnull Class<T> inputClass,
                @Nonnull Collection<T> inputs,
                @Nonnull NormalizeInputRule<T, NORM_IN> rule) {
            if (inputs.size() == 0) throw new IllegalArgumentException(" Input collection must not be empty! ");

            int index = findByInputClass( inputClass );
            new InputRulePair<T>(inputs, rule);
            pairsList.set(index, new InputRulePair<T>(inputs, rule));
            return this;
        }*/

        public <T extends AbstractInput> InputBuilder appendInputBatch(@Nonnull Collection<T> inputBatch) {
            if (inputBatch.size() == 0) throw new IllegalArgumentException(" Input collection must not be empty! ");

            int index = findByInputClass( inputBatch.iterator().next().getClass() );
            ((Collection<T>)pairsList.get(index).getKey()).addAll(inputBatch);
            return this;
        }

        public Input<NORM_IN> toInput() {
            return input;
        }
    }



    public Input() {

    }

    public Input(Input<NORM_IN> otherInput) {
        this();
        this.pairsList.addAll(otherInput.pairsList);
    }

    public <T extends AbstractInput> Input(@Nonnull Collection<T> inputs,
                                           @Nonnull NormalizeInputRule<T, NORM_IN> rule) {
        addInput(inputs, rule);
    }

    public Input(@Nonnull List<Collection<? extends AbstractInput>> inputList,
                 @Nonnull List<NormalizeInputRule<?, NORM_IN>> rulesList) {
        if (inputList.size() != rulesList.size()) {
            throw new IllegalArgumentException(" Both lists must have the same size! ");
        }

        for (int i = 0; i < inputList.size(); i++) {
            pairsList.add( new InputRulePair(inputList.get(i), rulesList.get(i)) );
        }
    }

    public Input(@Nonnull Map<NormalizeInputRule<? extends AbstractInput, NORM_IN>, Collection<? extends AbstractInput>> inputMap) {
        inputMap.entrySet().stream()
                .map(e -> new InputRulePair(e.getValue(), e.getKey()))
                .forEachOrdered(pairsList::add);
    }


    public <I extends AbstractInput> Input addInput(@Nonnull Collection<I> inputs,
                                                    @Nonnull NormalizeInputRule<I, NORM_IN> rule)
    {
        if (inputs.size() == 0) throw new IllegalArgumentException(" Input collection must not be empty! ");
        pairsList.add( new InputRulePair(inputs, rule) );
        return this;
    }



    public InputBuilder<NORM_IN> builder() {
        return new InputBuilder<>(this);
    }

    public boolean concat(Input<NORM_IN> anotherInput) {
        return this.pairsList.addAll( anotherInput.pairsList );
    }



    public List<NORM_IN> normalize() {
        return pairsList.parallelStream()
                        .map(p -> InputNormalizer.normalizeUnsafe(p.getValue(), p.getKey()))
                        .reduce(ListUtils::union)
                        .orElseGet(ArrayList::new);
    }
}
