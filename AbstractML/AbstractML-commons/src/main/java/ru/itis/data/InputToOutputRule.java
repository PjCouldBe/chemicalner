package ru.itis.data;

import java.util.List;

/**
 * Created by Dima on 13.07.2016.
 */
@FunctionalInterface
public interface InputToOutputRule<I extends AbstractInput, A, OUT extends AbstractOutput> {
    List<OUT> apply(List<I> inputs, OutputBatch<A> outputBatch);
}
