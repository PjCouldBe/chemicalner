package ru.itis.data;

import java.util.function.Function;

/**
 * Created by Dima on 10.07.2016.
 */
public interface NormalizeInputRule<RAW_IN extends AbstractInput, NORM_IN extends NormalizedInput>
        extends Function<RAW_IN, NORM_IN>
{

}
