package ru.itis.evaluate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Dima on 27.07.2016.
 */
@SuppressWarnings("PointlessBooleanExpression")
public interface ScriptEvaluationMethod extends IEvaluationMethod<String> {
    public static abstract class ScriptLaunchTool {
        public abstract String launchScript(String pathToScript, String... args);
    }

    public static class ShScriptTool extends ScriptLaunchTool {
        @Override
        public String launchScript(String pathToScript, String... args) {
            if (pathToScript.endsWith(".sh") == false) {
                throw new IllegalArgumentException(" There is not .sh script provided ");
            }

            return null;   //TODO
        }
    }

    public static class BatScriptTool extends ScriptLaunchTool {
        @Override
        public String launchScript(String pathToScript, String... args) {
            if (pathToScript.endsWith(".bat") == false) {
                throw new IllegalArgumentException(" There is not .sh script provided ");
            }

            return null;   //TODO
        }
    }

    public static class CMDScriptTool extends ScriptLaunchTool {
        @Override
        public String launchScript(String command, String... args) {
            ProcessBuilder builder = new ProcessBuilder(
                    "cmd.exe", "/c", command + " " + String.join(" ", args));
            builder.redirectErrorStream(true);

            StringBuilder evalResult = new StringBuilder();
            try {
                Process p = builder.start();
                try (BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
                    while (true) {
                        String l = r.readLine();
                        if (l == null) { break; }
                        evalResult.append(l).append("\n");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


            return evalResult.toString();
        }
    }

    public static class MACScriptTool extends ScriptLaunchTool {
        @Override
        public String launchScript(String pathToScript, String... args) {
            return null;   //TODO
        }
    }

    /*@Override
    default String convertToRequiredType(List<OUT> annotations) {
        File res = File.createTempFile(RandomStringUtils.random(10, true, true), "");
        return null;
    }*/
}
