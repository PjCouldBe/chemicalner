package ru.itis.evaluate;

import ru.itis.mark.Actual;
import ru.itis.mark.Expected;

import javax.annotation.Nonnull;

/**
 * Created by Dima on 10.07.2016.
 */
public abstract class EvaluationCharacteristic<T> {
    protected String name;

    public abstract <A> T apply(@Nonnull Expected<A> expected, @Nonnull Actual<A> actual);

    public String getName() {
        return name;
    }
}
