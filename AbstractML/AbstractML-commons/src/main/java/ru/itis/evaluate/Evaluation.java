package ru.itis.evaluate;

import gnu.trove.map.hash.TObjectIntHashMap;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Created by Dima on 10.07.2016.
 */
public class Evaluation {
    private TObjectIntHashMap<String> name2place = new TObjectIntHashMap<>();

    private int[] intEvals;
    private double[] doubleEvals;
    private Object[] objEvals;

    public Evaluation(@Nonnull TObjectIntHashMap<String> name2place,
                      @Nonnull int[] intEvals,
                      @Nonnull double[] doubleEvals,
                      @Nonnull Object[] objEvals) {
        checkEvaluationArgs(name2place, intEvals, doubleEvals, objEvals);

        this.name2place = name2place;
        this.intEvals = intEvals;
        this.doubleEvals = doubleEvals;
        this.objEvals = objEvals;
    }
    private void checkEvaluationArgs(TObjectIntHashMap<String> name2place,
                                     int[] intEvals,
                                     double[] doubleEvals,
                                     Object[] objEvals) {
        int intSize = 0;
        int doubleSize = 0;
        for (int v : name2place.values()) {
            int arrayId = v / 100;
            int elemId = v % 100;

            if (arrayId == 1) {
                intSize++;
            } else if (arrayId == 2) {
                doubleSize++;
            } else {
                if (objEvals[elemId] == null) {
                    throw new IllegalStateException(" The evaluation object in " + elemId + " position is null! ");
                }
            }
        }

        if (intEvals.length != intSize) {
            throw new IllegalStateException(" The int evaluation values array length is not equal " +
                    "to amount of integer related registers in nameToPlace map! " +
                    "Expected: " + intSize + ", got: " + intEvals.length);
        }
        if (doubleEvals.length != doubleSize) {
            throw new IllegalStateException(" The double evaluation values array length is not equal " +
                    "to amount of double related registers in nameToPlace map! " +
                    "Expected: " + doubleSize + ", got: " + doubleEvals.length);
        }
    }

    @SuppressWarnings({"unchecked", "PointlessBooleanExpression"})
    public <R> R getCharacteristic(String name) {
        if (name2place.containsKey(name) == false) return null;

        int id = name2place.get(name);
        int arrayId = id / 100;
        int elemId = id % 100;

        if (arrayId == 1) {
            return (R)(Integer)intEvals[elemId];
        } else if (arrayId == 2) {
            return (R)(Double)doubleEvals[elemId];
        } else {
            return (R)objEvals[elemId];
        }
    }

    @SuppressWarnings({"unchecked", "PointlessBooleanExpression"})
    public <R> R getPersentageCharacteristic(String name) {
        if (name2place.containsKey(name) == false) return null;

        int id = name2place.get(name);
        int arrayId = id / 100;
        int elemId = id % 100;

        if (arrayId == 1) {
            return (R)(Integer) (intEvals[elemId] * 100);
        } else if (arrayId == 2) {
            return (R)(Double) (doubleEvals[elemId] * 100);
        } else {
            return (R)objEvals[elemId];
        }
    }



    public void printAllCharacteristics() {
        printAll( System.out );
    }

    public void printAllCharacteristics(@Nonnull OutputStream os) {
        printAll( new PrintStream(os) );
    }

    private void printAll(PrintStream ps) {
       name2place.keySet().stream().forEach(k -> {
            int v = name2place.get(k);
            int arrayId = v / 100;
            int elemId = v % 100;

            Object o;
            if (arrayId == 1) {
                o = intEvals[elemId];
            } else if (arrayId == 2) {
                o = doubleEvals[elemId];
            } else {
                o = objEvals[elemId];
            }

            ps.println(StringUtils.capitalize(k) + " - " + o.toString());
        });

        if (ps != System.out) {
            ps.close();
        }
    }
}
