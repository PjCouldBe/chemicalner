package ru.itis.evaluate;

import ru.itis.mark.Actual;
import ru.itis.mark.Expected;

import javax.annotation.Nonnull;

/**
 * Created by Dima on 27.07.2016.
 */
public interface IEvaluationMethod<ARG_TYPE> {
    Evaluation evaluate(@Nonnull Expected<ARG_TYPE> expected, @Nonnull Actual<ARG_TYPE> actual);
}
