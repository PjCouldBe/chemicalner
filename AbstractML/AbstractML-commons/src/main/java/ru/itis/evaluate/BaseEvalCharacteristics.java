package ru.itis.evaluate;

import com.google.common.collect.Sets;

import ru.itis.mark.Actual;
import ru.itis.mark.Expected;

import java.util.Set;
import javax.annotation.Nonnull;

/**
 * Created by Dima on 10.07.2016.
 */
public class BaseEvalCharacteristics {
    //TRUE POSITIVE - all elements that are either in expected set or in actual
    public static final EvaluationCharacteristic<Integer> TP = new EvaluationCharacteristic<Integer>() {
        { name = "TP"; }

        @Override
        public <OUT> Integer apply(@Nonnull Expected<OUT> expected, @Nonnull Actual<OUT> actual) {
            Set<OUT> resultSet = Sets.newHashSet( expected.get() );
            resultSet.retainAll( Sets.newHashSet(actual.get()) );
            return resultSet.size();
        }
    };

    //FALSE POSITIVE - all elements that are in actual set but not in expected
    public static final EvaluationCharacteristic<Integer> FP = new EvaluationCharacteristic<Integer>() {
        { name = "FP"; }

        @Override
        public <OUT> Integer apply(@Nonnull Expected<OUT> expected, @Nonnull Actual<OUT> actual) {
            Set<OUT> expectedSet = Sets.newHashSet(expected.get());
            Set<OUT> actualSet = Sets.newHashSet(actual.get());
            actualSet.removeAll(expectedSet);
            return actualSet.size();
        }
    };

    //TRUE NEGATIVE - all elements that are in expected set but not in actual   //TODO: I don't know how to get it
    public static final EvaluationCharacteristic<Integer> TN = new EvaluationCharacteristic<Integer>() {
        { name = "TN"; }

        @Override
        public <OUT> Integer apply(@Nonnull Expected<OUT> expected, @Nonnull Actual<OUT> actual) {
            return 0;
        }
    };

    //FALSE NEGATIVE - all elements that are in expected set but not in actual
    public static final EvaluationCharacteristic<Integer> FN = new EvaluationCharacteristic<Integer>() {
        { name = "FN"; }

        @Override
        public <OUT> Integer apply(Expected<OUT> expected, Actual<OUT> actual) {
            Set<OUT> expectedSet = Sets.newHashSet(expected.get());
            Set<OUT> actualSet = Sets.newHashSet(actual.get());
            expectedSet.removeAll(actualSet);
            return expectedSet.size();
        }
    };

    //PRECISION - TP / (TP + FP)
    public static final EvaluationCharacteristic<Double> PRECISION = new EvaluationCharacteristic<Double>() {
        { name = "precision"; }

        @Override
        public <OUT> Double apply(Expected<OUT> expected, Actual<OUT> actual) {
            int tp = TP.apply(expected, actual);
            int fp = FP.apply(expected, actual);
            return (double)tp / (tp + fp);
        }
    };

    //RECALL - TP / (TP + FN)
    public static final EvaluationCharacteristic<Double> RECALL = new EvaluationCharacteristic<Double>() {
        { name = "recall"; }

        @Override
        public <OUT> Double apply(Expected<OUT> expected, Actual<OUT> actual) {
            int tp = TP.apply(expected, actual);
            int fn = FN.apply(expected, actual);
            return (double)tp / (tp + fn);
        }
    };

    //F1-measure - 2 * PRECISION * RECALL / (PRECISION + RECALL)
    public static final EvaluationCharacteristic<Double> F1 = new EvaluationCharacteristic<Double>() {
        { name = "F1"; }

        @Override
        public <OUT> Double apply(Expected<OUT> expected, Actual<OUT> actual) {
            //NOT optimized
            /*double prec = PRECISION.apply(expected, actual);
            double recall = RECALL.apply(expected, actual);
            return 2 * prec * recall / (prec + recall);*/

            //optimized
            int tp = TP.apply(expected, actual);
            return (double)tp * 2 / (expected.get().size() + actual.get().size());  //F1 = 2 * TP / (2 * TP + FP + FN);
                                                                       // 2 * TP + FP + FN = expected.size() + actual.size()
        }
    };
}
