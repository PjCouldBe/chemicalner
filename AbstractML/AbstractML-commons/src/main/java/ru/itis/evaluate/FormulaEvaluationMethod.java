package ru.itis.evaluate;

import gnu.trove.map.hash.TObjectIntHashMap;
import ru.itis.mark.Actual;
import ru.itis.mark.Expected;

import javax.annotation.Nonnull;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static ru.itis.evaluate.BaseEvalCharacteristics.*;

/**
 * Created by Dima on 27.07.2016.
 */
public class FormulaEvaluationMethod<OUT> implements IEvaluationMethod<OUT> {
    private EvaluationCharacteristic<?>[] evals = new EvaluationCharacteristic[10];
    private int maxIndex = 0;

    public FormulaEvaluationMethod addEvaluationCharacteristic(EvaluationCharacteristic<?> eval) {
        if (maxIndex >= evals.length) {
            EvaluationCharacteristic<?>[] temp = evals;
            evals = new EvaluationCharacteristic<?>[temp.length * 2];
            System.arraycopy(temp, 0, evals, 0, temp.length);
        }

        evals[maxIndex] = eval;
        maxIndex++;
        return this;
    }

    public FormulaEvaluationMethod configEvalCharacteristics(EvaluationCharacteristic<?>... evals) {
        this.evals = evals;
        maxIndex = evals.length;
        return this;
    }

    public static FormulaEvaluationMethod configDefaultEvalCharacteristics() {
        FormulaEvaluationMethod method = new FormulaEvaluationMethod();
        method.configEvalCharacteristics(TP, FP, TN, FN, PRECISION, RECALL, F1);
        return method;
    }

    public void clear() {
        evals = new EvaluationCharacteristic[0];
    }

    public EvaluationCharacteristic<?>[] getCharacteristics() {
        return evals;
    }


    @Override
    public Evaluation evaluate(@Nonnull Expected<OUT> expected, @Nonnull Actual<OUT> actual) {
        TObjectIntHashMap<String> name2place = new TObjectIntHashMap<>();

        int[] intEvals;
        double[] doubleEvals;
        Object[] objEvals;

        Map<Class<?>, List<AbstractMap.SimpleEntry<String, Object>>> groupedEvalResults =
                Stream.of( evals )
                      .map(e -> new AbstractMap.SimpleEntry<String, Object>(e.getName(), e.apply(expected, actual)))
                      .collect( Collectors.groupingBy(result -> {
                                    Class<?> resultClass = result.getValue().getClass();
                                    return resultClass == Integer.class
                                            ? Integer.class
                                            : (resultClass == Double.class) ? Double.class : Object.class;
                                })
                        );
        intEvals = new int[ getSize( groupedEvalResults, Integer.class) ];
        doubleEvals = new double[ getSize( groupedEvalResults, Double.class) ];
        objEvals = new Object[ getSize( groupedEvalResults, Object.class) ];

        for (Map.Entry<Class<?>, List<AbstractMap.SimpleEntry<String, Object>>> entry : groupedEvalResults.entrySet()) {
            Class<?> entryClass = entry.getKey();

            int iInt = 0, iDouble = 0, iObj = 0;
            //1 - for Ints, 2 - for Doubles, 3 - for rest Object types
            for (AbstractMap.SimpleEntry<String, Object> elem : entry.getValue()) {
                if (entryClass == Integer.class) {
                    intEvals[iInt] = (Integer)elem.getValue();
                    name2place.put(elem.getKey(), 1 * 100 + iInt);
                    iInt++;
                } else if (entryClass == Double.class) {
                    doubleEvals[iDouble] = (Double)elem.getValue();
                    name2place.put(elem.getKey(), 2 * 100 + iDouble);
                    iDouble++;
                } else {
                    objEvals[iObj] = elem.getValue();
                    name2place.put(elem.getKey(), 3 * 100 + iObj);
                    iObj++;
                }
            }
        }

        return new Evaluation(name2place, intEvals, doubleEvals, objEvals);
    }
    private int getSize(Map<Class<?>, List<AbstractMap.SimpleEntry<String, Object>>> groupedEvalResults, Class<?> clazz) {
        List<AbstractMap.SimpleEntry<String, Object>> lst = groupedEvalResults.get(clazz);
        return lst == null ? 0 : lst.size();
    }
}
