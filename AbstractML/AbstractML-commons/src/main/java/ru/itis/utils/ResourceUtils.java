package ru.itis.utils;

import java.io.File;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Optional;

/**
 * Created by Dima on 13.07.2016.
 */
public class ResourceUtils {
    public static InputStream getResource(String path) {
        return ResourceUtils.class.getClassLoader().getResourceAsStream(path);
    }

    public static File getResourceAsFile(String path) {
        try {
            return new File( ResourceUtils.class.getClassLoader().getResource(path).toURI() );
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getResourceAsPath(String path) {
        return Optional.ofNullable(getResourceAsFile(path)).map(f -> f.getPath()).orElse(null);
    }
}
