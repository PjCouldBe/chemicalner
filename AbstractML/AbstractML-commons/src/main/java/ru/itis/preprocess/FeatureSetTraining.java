package ru.itis.preprocess;

import ru.itis.data.NormalizedInput;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Dima on 28.07.2016.
 */
public class FeatureSetTraining<N extends NormalizedInput, OUT> extends FeatureSet<N> {
    public class FeatureSetBatchTraining<N extends NormalizedInput, OUT> extends FeatureSet<N>.FeatureSetBatch<N> {
        FeatureSetBatchTraining(Object[] output) {
            super(output);
        }

        FeatureSetBatchTraining(FeatureSetBatch<N> fsb) {
            super(fsb.output);
        }

        public OUT[] getTtrainingData() {
            return (OUT[])output[output.length - 1];
        }
    }

    public FeatureSetTraining(@Nonnull NLPPipeline<N> nlpPipeline,
                              @Nonnull Collection<N> inputs,
                              Collection<OUT[]> trainingData) {
        super(nlpPipeline, inputs);

        ArrayList<?>[] temp = new ArrayList<?>[this.outputs.length + 1];
        System.arraycopy(this.outputs, 0, temp, 0, this.outputs.length);
        temp[temp.length - 1] = trainingData == null ? null : new ArrayList<>(trainingData);
        this.outputs = temp;
    }

    @Override
    public <T extends FeatureSetBatch<N>> T getFeatureSetBatch(int i) {
        return (T)new FeatureSetBatchTraining(super.getFeatureSetBatch(i));
    }

    @Override
    public <T extends FeatureSetBatch<N>> T getFeatureSetBatch(N input) {
        return (T)new FeatureSetBatchTraining(super.getFeatureSetBatch(input));
    }
}
