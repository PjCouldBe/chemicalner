package ru.itis.preprocess;

import ru.itis.data.NormalizedInput;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Dima on 10.07.2016.
 */
public class FeatureSet<N extends NormalizedInput> {
    protected Class<?>[] types;
    protected ArrayList<?>[] outputs;

    public class FeatureSetBatch<N extends NormalizedInput> {
        protected Object[] output;

        FeatureSetBatch(Object[] output) {
            this.output = output;
        }

        public <R> R getSpecifiedParameter(Class<R> parameterClass) {
            int i = 0;
            for ( ; i < types.length; i++) {
                if (parameterClass == types[i]) break;
            }

            if (i >= types.length) return null;

            return (R)this.output[i];
        }

        public N getInputData() {
            return (N)output[0];
        }
    }

    public FeatureSet(@Nonnull NLPPipeline<N> nlpPipeline,
                      @Nonnull Collection<N> inputs) {
        nlpPipeline.trimAllArrays();
        this.types = nlpPipeline.types.toArray( new Class<?>[nlpPipeline.types.size()] );
        this.outputs = extractOuputs(nlpPipeline, new ArrayList<>(inputs));
    }

    private <T extends NormalizedInput> ArrayList<?>[] extractOuputs(
            NLPPipeline<T> nlpPipeline, ArrayList<T> inputs) {
        ArrayList<?>[] resultOutputs = new ArrayList<?>[types.length];
        resultOutputs[0] = inputs;

        for (int i = 1; i < types.length; i++) {
            int[] argPointers = nlpPipeline.argumentPointers.get(i);
            List<?>[] args = new ArrayList<?>[argPointers.length];
            for (int j = 0; j < args.length; j++) {
                args[j] = resultOutputs[ argPointers[j] ];
            }

            resultOutputs[i] = nlpPipeline.handlers.get(i).handle(args);
            resultOutputs[i].trimToSize();
        }

        return resultOutputs;
    }

    public <T extends FeatureSetBatch<N>> T getFeatureSetBatch(N input) {
        int i = 0;
        for (N in : (ArrayList<N>)outputs[0]) {
            if (in.equals(input)) break;
            i++;
        }

        return (i >= outputs[0].size()) ? null : getFeatureSetBatch(i);
    }

    public <T extends FeatureSetBatch<N>> T getFeatureSetBatch(int i) {
        Object[] data = new Object[outputs.length];
        for (int j = 0; j < outputs.length; j++) {
            data[j] = outputs[j].get(i);
        }

        return (T)new FeatureSetBatch(data);
    }

    public int size() {
        return outputs[0].size();
    }

    public <T> ArrayList<T> getSpecifiedParameters(Class<T> parameterClass) {
        int i = 0;
        for ( ; i < types.length; i++) {
            if (parameterClass == types[i]) break;
        }

        if (i >= types.length) return null;

        return (ArrayList<T>)outputs[i];
    }


    public <T extends FeatureSetBatch<N>> T[] toArray() {
        int i = 0;
        FeatureSetBatch[] res = new FeatureSetBatch[ size() ];

        for (; i < size(); i++) {
            res[i] = getFeatureSetBatch(i);
        }
        return (T[])res;
    }
}
