package ru.itis.preprocess;

import com.google.common.reflect.TypeToken;

import ru.itis.data.NormalizedInput;

import javax.annotation.Nonnull;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;

/**
 * Created by Dima on 10.07.2016.
 */
public class NLPPipeline<N extends NormalizedInput> {
    ArrayList<Class<?>> types = new ArrayList<>();
    ArrayList<int[]> argumentPointers = new ArrayList<>();
    ArrayList<Handler<?>> handlers = new ArrayList<>();
    private boolean trimmed = false;

    public <T extends NormalizedInput> NLPPipeline(Class<T> normalizeInputClass) {
        types.add(normalizeInputClass);

        argumentPointers.add( new int[0] );
        handlers.add(null);
    }

    public <T> NLPPipeline addHandler(@Nonnull Handler<T> handler,
                                      @Nonnull Class<T> handlerClass,
                                      @Nonnull Class<?>[] handlerArgumentsTypes)
    {
        //add to 2-nd array, in the beginning because of
        //making the operation safe from irrevocable actions
        int i = 0;
        int[] classPointers = new int[handlerArgumentsTypes.length];
        for (Class<?> c : handlerArgumentsTypes) {
            int j = 0;
            for (Class<?> c1 : types) {
                if (c1 == c) {
                    classPointers[i] = j;
                    break;
                }
                j++;
            }
            i++;
        }
        argumentPointers.add(classPointers);

        //add to 1-st array
        types.add(handlerClass);

        //add to 3-rd array
        handlers.add(handler);

        trimmed = false;
        return this;
    }

    void trimAllArrays() {
        if (types.size() != argumentPointers.size() || types.size() != handlers.size()) {
            throw new IllegalStateException();
        }

        if (trimmed) return;

        types.trimToSize();
        argumentPointers.trimToSize();
        handlers.trimToSize();
        trimmed = true;
    }
}
