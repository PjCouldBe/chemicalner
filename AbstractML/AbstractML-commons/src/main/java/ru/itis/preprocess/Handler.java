package ru.itis.preprocess;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dima on 11.07.2016.
 */
@FunctionalInterface
public interface Handler<T> {
    ArrayList<T> handle (List<?>... args);
}
