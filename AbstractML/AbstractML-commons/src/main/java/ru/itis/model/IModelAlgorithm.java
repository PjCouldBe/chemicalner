package ru.itis.model;

import ru.itis.data.OutputBatch;
import ru.itis.data.NormalizedInput;
import ru.itis.preprocess.FeatureSet;
import ru.itis.preprocess.FeatureSetTraining;

/**
 * Created by Dima on 24.07.2016.
 */
public interface IModelAlgorithm<N extends NormalizedInput, OUT> {
    void learn(FeatureSetTraining<N, OUT> featureSet);

    <BATCH extends OutputBatch<OUT>> BATCH apply(FeatureSet featureSet);
}

