package ru.itis.model;

import ru.itis.data.*;
import ru.itis.evaluate.Evaluation;
import ru.itis.evaluate.FormulaEvaluationMethod;
import ru.itis.evaluate.IEvaluationMethod;
import ru.itis.mark.Actual;
import ru.itis.mark.Expected;
import ru.itis.preprocess.FeatureSet;
import ru.itis.preprocess.NLPPipeline;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.function.Function;

/**
 * Created by Dima on 13.07.2016.
 */
public class TrainedModel<N extends NormalizedInput, OUT, ARG> {
    private final IModelAlgorithm<N, OUT> alg;
    private final NLPPipeline<N> nlp;
    private IEvaluationMethod<ARG> evalMethod;

    /**
     * The constructir with default evaluation configuration
     *
     * @param nlp
     * @param alg
     */
    public TrainedModel(@Nonnull NLPPipeline<N> nlp,
                        @Nonnull IModelAlgorithm<N, OUT> alg)
    {
        this.nlp = nlp;
        this.alg = alg;
        this.evalMethod = (IEvaluationMethod<ARG>)getDefaultEvalConfig();
    }

    private IEvaluationMethod<OUT> getDefaultEvalConfig() {
        return FormulaEvaluationMethod.configDefaultEvalCharacteristics();
    }

    public void setEvalMethod(@Nonnull IEvaluationMethod evalMethod) {
        this.evalMethod = evalMethod;
    }


    public <I extends AbstractInput> Evaluation evaluate(
            @Nonnull Input<N> inputData,
            List<ARG> expectedEvalData,
            Function<List<OUT>, List<ARG>> evalConverter) {
        List<OUT> batch = alg.apply(new FeatureSet(nlp, inputData.normalize() )).getOutputAnnotations();
        return evalMethod.evaluate( new Expected(expectedEvalData), new Actual( evalConverter.apply(batch) ) );
    }

    public <I extends AbstractInput> List<OUT> apply(@Nonnull Input<N> input)
    {
        OutputBatch batch = alg.apply(new FeatureSet(nlp, input.normalize() ));
        return batch.getOutputAnnotations();
    }

    public <O extends AbstractOutput, I extends AbstractInput>
                          List<O> use(@Nonnull List<I> targetSet,
                                      @Nonnull NormalizeInputRule<I,N> rule,
                                      @Nonnull InputToOutputRule<I, OUT, O> ruleToOutput)
    {
        return use(targetSet, new NormalizeInputRule[] {rule}, ruleToOutput);
    }

    public <O extends AbstractOutput, I extends AbstractInput>
    List<O> use(@Nonnull List<I> targetSet,
                @Nonnull NormalizeInputRule<I,N>[] rules,
                @Nonnull InputToOutputRule<I, OUT, O> ruleToOutput)
    {
        Input input = new Input();
        for (NormalizeInputRule<I,N> r : rules) {
            input.addInput(targetSet, r);
        }

        OutputBatch<OUT> out = alg.apply( new FeatureSet(nlp, input.normalize() ));
        return ruleToOutput.apply(targetSet, out);
    }
}
