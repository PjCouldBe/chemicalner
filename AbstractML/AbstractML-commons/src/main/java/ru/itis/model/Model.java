package ru.itis.model;

import ru.itis.mark.Expected;
import ru.itis.data.NormalizedInput;
import ru.itis.preprocess.FeatureSetTraining;
import ru.itis.preprocess.NLPPipeline;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Created by Dima on 10.07.2016.
 */
public class Model {

    public static <N extends NormalizedInput, OUT, ARG> TrainedModel<N, OUT, ARG>
                                                                train(@Nonnull List<N> input,
                                                                      @Nonnull NLPPipeline<N> nlpPipeline,
                                                                      @Nonnull IModelAlgorithm<N, OUT> alg,
                                                                      @Nonnull Expected<OUT[]> expected)
    {
        alg.learn( new FeatureSetTraining<>(nlpPipeline, input, expected.get()) );
        return new TrainedModel<>(nlpPipeline, alg);
    }
}
