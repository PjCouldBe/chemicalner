package ru.itis.mark;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Created by Filippov on 12.07.2016.
 */
public class Expected<T> {
    private final List<T> value;

    public Expected(@Nonnull List<T> value) {
        this.value = value;
    }

    public List<T> get() {
        return value;
    }
}
