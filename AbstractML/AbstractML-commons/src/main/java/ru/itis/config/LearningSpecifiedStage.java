package ru.itis.config;

import ru.itis.learning.ILearningMethod;
import ru.itis.model.IModelAlgorithm;
import ru.itis.data.NormalizedInput;

import javax.annotation.Nonnull;

/**
 * Created by Dima on 27.07.2016.
 */
public class LearningSpecifiedStage<OUT, N extends NormalizedInput> extends AbstractMLPipelineStage {
    ILearningMethod<N, OUT> learningMethod;

    LearningSpecifiedStage(@Nonnull ILearningMethod<N, OUT> learningMethod) {
        super();
        this.learningMethod = learningMethod;
    }

    public AlgorithmSpecifiedStage<OUT, N> configureLearningAlgorithm(@Nonnull IModelAlgorithm<N, OUT> alg) {
        return new AlgorithmSpecifiedStage<>(learningMethod, alg);
    }
}
