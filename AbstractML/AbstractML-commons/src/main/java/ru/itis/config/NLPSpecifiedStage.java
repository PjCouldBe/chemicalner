package ru.itis.config;

import ru.itis.data.NormalizedInput;
import ru.itis.evaluate.IEvaluationMethod;
import ru.itis.learning.ILearningMethod;
import ru.itis.model.IModelAlgorithm;
import ru.itis.preprocess.NLPPipeline;

import javax.annotation.Nonnull;

/**
 * Created by Filippov on 28.07.2016.
 */
public class NLPSpecifiedStage<OUT, N extends NormalizedInput> extends AbstractMLPipelineStage {
    ILearningMethod<N, OUT> learningMethod;
    IModelAlgorithm<N, OUT> alg;
    NLPPipeline<N> nlp;

    NLPSpecifiedStage(@Nonnull ILearningMethod<N, OUT> learningMethod,
                      @Nonnull IModelAlgorithm<N, OUT> alg,
                      @Nonnull NLPPipeline<N> nlp) {
        this.learningMethod = learningMethod;
        this.alg = alg;
        this.nlp = nlp;
    }

    public <ARG> EvaluationSpecifiedStage<OUT, N, ARG> configureEvaluation(@Nonnull IEvaluationMethod<ARG> eval) {
        return new EvaluationSpecifiedStage<>(learningMethod, alg, nlp, eval);
    }

    public MLPipeline endConfiguration() {
        return new MLPipeline(
                new MLPipelineConfiguration<>(learningMethod, alg, nlp) );
    }
}
