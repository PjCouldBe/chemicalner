package ru.itis.config;

/**
 * Created by Dima on 27.07.2016.
 */
public class EmptyStage extends AbstractMLPipelineStage {
    public <OUT> GoaledStage<OUT> configureGoal(Class<OUT> outClass) { return new GoaledStage<>(); }
}
