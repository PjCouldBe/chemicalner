package ru.itis.config;

import javax.annotation.Nonnull;

import ru.itis.data.NormalizedInput;
import ru.itis.learning.ILearningMethod;
import ru.itis.model.IModelAlgorithm;
import ru.itis.preprocess.NLPPipeline;

/**
 * Created by Filippov on 28.07.2016.
 */
public class AlgorithmSpecifiedStage<OUT, N extends NormalizedInput> extends AbstractMLPipelineStage {
    ILearningMethod<N, OUT> learningMethod;
    IModelAlgorithm<N, OUT> alg;

    AlgorithmSpecifiedStage(@Nonnull ILearningMethod<N, OUT> learningMethod,
                            @Nonnull IModelAlgorithm<N, OUT> alg) {
        this.learningMethod = learningMethod;
        this.alg = alg;
    }

    public NLPSpecifiedStage<OUT, N> configureNLPPipeline(@Nonnull NLPPipeline<N> nlp) {
        return new NLPSpecifiedStage<>(learningMethod, alg, nlp);
    }
}
