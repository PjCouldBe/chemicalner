package ru.itis.config;

import ru.itis.data.NormalizedInput;
import ru.itis.evaluate.IEvaluationMethod;
import ru.itis.learning.ILearningMethod;
import ru.itis.model.IModelAlgorithm;
import ru.itis.preprocess.NLPPipeline;

import javax.annotation.Nonnull;

/**
 * Created by Filippov on 28.07.2016.
 */
public class EvaluationSpecifiedStage<OUT, N extends NormalizedInput, ARG> extends  AbstractMLPipelineStage {
    ILearningMethod<N, OUT> learningMethod;
    IModelAlgorithm<N, OUT> alg;
    NLPPipeline<N> nlp;
    IEvaluationMethod<ARG> eval;

    EvaluationSpecifiedStage(@Nonnull ILearningMethod<N, OUT> learningMethod,
                             @Nonnull IModelAlgorithm<N, OUT> alg,
                             @Nonnull NLPPipeline<N> nlp,
                             @Nonnull IEvaluationMethod<ARG> eval) {
        this.learningMethod = learningMethod;
        this.alg = alg;
        this.nlp = nlp;
        this.eval = eval;
    }

    public MLPipeline endConfiguration() {
        return new MLPipeline(
                new MLPipelineConfiguration<>(learningMethod, alg, nlp, eval) );
    }
}
