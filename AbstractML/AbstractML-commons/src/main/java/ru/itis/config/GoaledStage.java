package ru.itis.config;

import ru.itis.data.NormalizedInput;
import ru.itis.data.Input;

/**
 * Created by Dima on 27.07.2016.
 */
public class GoaledStage<OUT> extends AbstractMLPipelineStage {
    public <N extends NormalizedInput> InputSpecifiedStage<OUT, N> configureInputs(Input<N> input) {
        return new InputSpecifiedStage<>(input);
    }
}
