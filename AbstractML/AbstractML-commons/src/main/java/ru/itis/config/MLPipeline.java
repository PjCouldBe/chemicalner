package ru.itis.config;

import ru.itis.data.NormalizedInput;
import ru.itis.model.TrainedModel;

/**
 * Created by Dima on 10.07.2016.
 */
@SuppressWarnings("unchecked")
public class MLPipeline {
    private MLPipelineConfiguration<?, ?, ?> config;

    public MLPipeline(MLPipelineConfiguration<?, ?, ?> config) {
        this.config = config;
    }

    public <R extends NormalizedInput, OUT, ARG> TrainedModel<R, OUT, ARG> run() {
        return (TrainedModel<R, OUT, ARG>)config.run();
    }
}
