package ru.itis.config;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import ru.itis.data.Input;
import ru.itis.data.NormalizedInput;
import ru.itis.learning.ILearningMethod;

import javax.annotation.Nonnull;

/**
 * Created by Dima on 27.07.2016.
 */
public class InputSpecifiedStage<OUT, N extends NormalizedInput> extends AbstractMLPipelineStage {
    Input<N> input = null;

    InputSpecifiedStage(@Nonnull Input<N> input) {
        super();
        this.input = input;
    }

    public LearningSpecifiedStage<OUT, N>
            confgureLearningMethod(List<OUT[]> trainingAnnotations,
                                   @Nonnull ILearningMethod<N, OUT> method) {
        method.setData(input.normalize(), trainingAnnotations);
        return new LearningSpecifiedStage(method);
    }
}
