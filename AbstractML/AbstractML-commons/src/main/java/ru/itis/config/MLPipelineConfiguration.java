package ru.itis.config;

import ru.itis.data.NormalizedInput;
import ru.itis.evaluate.IEvaluationMethod;
import ru.itis.learning.ILearningMethod;
import ru.itis.mark.Expected;
import ru.itis.model.IModelAlgorithm;
import ru.itis.model.Model;
import ru.itis.model.TrainedModel;
import ru.itis.preprocess.NLPPipeline;

import javax.annotation.Nonnull;

/**
 * Created by Dima on 27.07.2016.
 */
public class MLPipelineConfiguration<N extends NormalizedInput, OUT, ARG> {
    ILearningMethod<N, OUT> learningMethod;
    IModelAlgorithm<N, OUT> alg;
    NLPPipeline<N> nlp;
    IEvaluationMethod<ARG> eval;

    @SuppressWarnings("FinalStaticMethod")
    public static final EmptyStage create() {
        return new EmptyStage();
    }

    MLPipelineConfiguration(ILearningMethod<N, OUT> learningMethod,
                            IModelAlgorithm<N, OUT> alg,
                            NLPPipeline<N> nlp) {
        this(learningMethod, alg, nlp, null);
    }

    MLPipelineConfiguration(@Nonnull ILearningMethod<N, OUT> learningMethod,
                            @Nonnull IModelAlgorithm<N, OUT> alg,
                            @Nonnull NLPPipeline<N> nlp,
                            IEvaluationMethod eval) {
        this.learningMethod = learningMethod;
        this.alg = alg;
        this.nlp = nlp;
        this.eval = eval;
    }

    TrainedModel<N, OUT, ARG> run() {
        TrainedModel<N, OUT, ARG> result = Model.train(learningMethod.getInputs(), nlp,
                alg, new Expected<>(learningMethod.getTrainingAnnotations()) );
        if (eval != null) {
            result.setEvalMethod(eval);
        }

        return result;
    }
}
