package ru.itis.config;

import ru.itis.mark.Annotation;

/**
 * Created by Dima on 28.07.2016.
 */
public class NERPipeline {
    public static <A extends Annotation> GoaledStage<A> create(Class<A> annotationClass) {
        return MLPipelineConfiguration.create().configureGoal(annotationClass);
    }
}
