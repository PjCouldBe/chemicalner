package ru.itis.mark;

import java.util.Objects;

/**
 * Created by Dima on 09.07.2016.
 */
public abstract class Annotation {
    protected int start;
    protected int end;
    protected final String text;

    public Annotation(int start, int end, String text) {
        if (start > end) {
            throw new IllegalArgumentException("Possibly start and end arguments are in the incorrect order!");
        }

        this.start = start;
        this.end = end;
        this.text = Objects.requireNonNull(text);
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public String getOriginalText() {
        return text;
    }

    @Override
    public String toString() {
        return "Annotation (text = " + text
                + ", start = " + start
                + ", end = " + end + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Annotation that = (Annotation) o;

        if (start != that.start) return false;
        return end == that.end && text.equals(that.text);
    }

    @Override
    public int hashCode() {
        int result = start;
        result = 31 * result + end;
        result = 31 * result + text.hashCode();
        return result;
    }
}
